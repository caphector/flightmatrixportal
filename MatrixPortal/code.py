import time
import board
import terminalio
import json
from adafruit_matrixportal.matrixportal import MatrixPortal
from adafruit_matrixportal.network import Network
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from adafruit_bitmap_font import bitmap_font
from secrets import secrets
import adafruit_logging as logging
import random

SCROLL_DELAY = 0.07
seconds = 3

ARROWED = "▸"

AIRPORT_CODES = [
    "BOO",
    "BRR",
    "BUM",
    "CAT",
    "DIE",
    "DIK",
    "DOG",
    "EEK",
    "FAB",
    "FAT",
    "FUN",
    "GAG",
    "GRR",
    "HOG",
    "IOU",
    "MAD",
    "LOL",
    "OMG",
    "PEE",
    "PIE",
    "POO",
    "SUX",
    "UMM",
    "WOW",
    "YUM",
]

LARGE_FONT = "/fonts/helvB12.bdf"
SMALL_FONT = "/fonts/helvR10.bdf"
SMALLER_FONT = "/fonts/6x9.bdf"
SYMBOL_FONT = "/fonts/6x13.bdf"

matrixportal = MatrixPortal(status_neopixel=board.NEOPIXEL, debug=False)
network = matrixportal.network

MQTT.set_socket(socket, network._wifi.esp)

# (ID = 0) (Flight Initial Airport)
matrixportal.add_text(
    text_font=LARGE_FONT,
    text_position=(0, 2),
    text_color=0x800080,
)

# ID = 1 (▸)
matrixportal.add_text(
    text_font=SMALLER_FONT,
    text_position=(29, 4),
    text_color=0x000080,
)

# ID = 2 (Flight Destination)
matrixportal.add_text(
    text_font=LARGE_FONT,
    text_position=(35, 2),
    text_color=0x008080,
)

# ID = 3 (unused)
matrixportal.add_text(
    text_font=SMALL_FONT,
    text_position=(56, 6),
    text_color=0x008080,
)

# ID = 4 (Airplane Info Scroller)
matrixportal.add_text(
    text_font=LARGE_FONT,
    text_position=(2, 24),
    text_color=0x000080,
    scrolling=True,
)

# ID = 5 (Flight Info)
matrixportal.add_text(
    text_font=SMALLER_FONT,
    text_position=(1, 14),
    text_color=0x050080,
    scrolling=False,
)

ZONES = {
    "origin": "0",
    "separator": "1",
    "destination": "2",
    "empty_field": "3",
    "plane": "4",
    "flight_info": "5",
}

for key, value in ZONES.items():
    print("{}".format(key))


def get_key(field):
    if field in ZONES.keys():
        key = int(ZONES.get(field))
    return key


def set_text(my_text, field):
    key = get_key(field)
    print("text {} field {}".format(my_text, field))
    if field == "plane":
        set_text_scroll(my_text, field)
    matrixportal.set_text(my_text, key)


def set_route(origin, destination):
    set_text(origin, "origin")
    set_text(destination, "destination")
    set_text(ARROWED, "separator")


def set_text_scroll(my_text, field):
    key = get_key(field)
    print(field, key, my_text)
    matrixportal.set_text(my_text, key)
    mylength = len(my_text)


def display_from_message(
    origin, destination, flight, plane, vertical_speed, altitude, hex
):
    set_route(origin, destination)
    set_text_scroll(plane, "plane")
    if not flight:
        flight = hex
    set_text("{} {} {}".format(altitude, flight, vertical_speed), "flight_info")


set_route(random.choice(AIRPORT_CODES), random.choice(AIRPORT_CODES))
set_text("▸", "separator")
set_text(" ", "flight_info")
set_text("Connecting", 'flight_info')
set_text_scroll("Starting up…", "plane")
matrixportal.scroll_text(SCROLL_DELAY)

network.connect()

mqtt = MQTT.MQTT(
    broker=secrets.get("mqtt_broker"),
    username=secrets.get("mqtt_user"),
    password=secrets.get("mqtt_password"),
    port=1883,
)


def get_last_data(feed):
    feed_url = feeds.get(feed)
    return last_data.get(feed_url)


def message_received(client, topic, message):
    print("Received {} for {}".format(message, topic))
    plane = json.loads(message)
    if all(value == '' for value in plane.values()):
        wait_and_blank(seconds)
    else:
        display_from_message(**plane)
        wait_without_blank(90)


def subscribe():
    try:
        mqtt.is_connected()
    except MQTT.MMQTTException:
        mqtt.connect()
    mqtt.subscribe("flight/0")
    print("Subscribed")
    mqtt.on_message = message_received


def wait_and_blank(wait_time):
    if not wait_time:
        wait_time == seconds
    print("Waiting {}".format(wait_time))
    end_time = time.time() + wait_time
    while time.time() < end_time:
        matrixportal.scroll_text(SCROLL_DELAY)
        time.sleep(3)
    print("Blanking display...")
    for key, value in ZONES.items():
        print("{}".format(key))
        set_text(" ", key)


def wait_without_blank(wait_time):
    if not wait_time:
        wait_time == seconds
    print("Waiting for message {}".format(wait_time))
    end_time = time.time() + wait_time
    while time.time() < end_time:
        matrixportal.scroll_text(SCROLL_DELAY)
        time.sleep(3)


subscribe()

while True:
    try:
        mqtt.is_connected()
        mqtt.loop()
        matrixportal.scroll_text(SCROLL_DELAY)
        wait_and_blank(seconds)
    except (MQTT.MMQTTException, RuntimeError, ConnectionError):
        network.connect()
        try:
            mqtt.reconnect()
        except MMQTTException:
            mqtt.reconnect()
        print("Reconnected")
