#!/usr/bin/env python3
# <bitbar.title>Nearest Plane</bitbar.title>
# <bitbar.version>v0.1</bitbar.version>
# <bitbar.author>Duncan</bitbar.author>
# <bitbar.author.gitlab>caphector</bitbar.author.gitlab>
# <bitbar.desc>Reads MQTT data about airplanes</bitbar.desc>
# <bitbar.dependencies>python</bitbar.dependencies>
# <bitbar.abouturl>http://url-to-about.com/</bitbar.abouturl>
# <swiftbar.type>streamable</swiftbar.type>

from configparser import SafeConfigParser, ConfigParser
import pathlib
from os.path import exists
import random
import json
from paho.mqtt import client as mqtt_client

config_path = pathlib.Path(__file__).parent.absolute() / ".config.ini"
settings = ConfigParser()
settings.read(config_path)

MQTT_CLIENT_ID = f'swiftbar-{random.randint(0, 1000)}'


def connect_mqtt():
    """Connect to MQTT"""
    def on_connect(client, userdata, flags, rc):
        client.subscribe(settings['mqtt']['topic'])
        if rc == 0:
            pass
        else:
            pass
    client = mqtt_client.Client(MQTT_CLIENT_ID)
    client.username_pw_set(settings['mqtt']['user'], settings['mqtt']['pass'])
    client.connect(settings['mqtt']['server'], 1883)
    client.subscribe(settings['mqtt']['topic'])
    return client


def on_message(client, userdata, msg):
    plane = json.loads(msg.payload)
    if all(value == '' for value in plane.values()):
        output = "~~~\n✈\n"
        print(output, flush=True)
        return
    else:
        display(**plane)


def display(origin, destination, flight, plane, vertical_speed, altitude, hex):
    if origin == '!!!':
        data = plane.replace(" - ", "\n")
        print(f'~~~\n{altitude}\n---\n{data}\n{altitude}', flush=True)
        return
    else:
        output = f"~~~\n{origin} ▶ {destination}\n---\n{plane}\n{altitude}\n"
        print(output, flush=True)
        return


MQTT_CLIENT = connect_mqtt()


if __name__ == "__main__":
    MQTT_CLIENT = connect_mqtt()
    while True:
        try:
            MQTT_CLIENT.is_connected()
            MQTT_CLIENT.loop()
            MQTT_CLIENT.on_message = on_message
        except (RuntimeError, ConnectionError):
            MQTT_CLIENT.reconnect()
