# tests/conftest.py
import pytest
from datetime import datetime
from models.aircraft import Aircraft
from models.flight import Flight

@pytest.fixture
def sample_aircraft_data():
    return {
        "icao24": "abc123",
        "callsign": "UAL123",
        "latitude": 34.0522,
        "longitude": -118.2437,
        "altitude": 35000,
        "velocity": 450.0
    }

@pytest.fixture
def sample_flight_data():
    return {
        "flight_number": "UAL123",
        "origin": "LAX",
        "destination": "JFK",
        "aircraft_type": "B738"
    }

@pytest.fixture
def mock_apis(mocker):
    return {
        "opensky": mocker.patch("api.opensky_api.OpenSkyAPI"),
        "flightaware": mocker.patch("api.flightaware_api.FlightAwareAPI"),
        "adsb": mocker.patch("api.adsb_db.ADSBdbAPI")
    }

@pytest.fixture
def mock_mqtt(mocker):
    return mocker.patch("core.mqtt_handler.MQTTHandler")
