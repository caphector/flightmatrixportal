import pytest
import json
from datetime import datetime
from models.flight import Flight

@pytest.fixture
def sample_flight():
    return Flight(
        origin="LAX",
        destination="JFK",
        flight_number="AA123",
        aircraft_type="B738",
        airline="American Airlines",
        vertical_speed=0,
        altitude=35000,
        hex="AB1234",
        registration="N12345",
        ground_speed=450,
        heading=90,
        latitude=34.0522,
        longitude=-118.2437,
        last_updated=datetime.now()
    )

def test_flight_creation(sample_flight):
    assert sample_flight.origin == "LAX"
    assert sample_flight.destination == "JFK"
    assert sample_flight.flight_number == "AA123"
    assert sample_flight.aircraft_type == "B738"
    assert sample_flight.airline == "American Airlines"
    assert sample_flight.altitude == 35000
    assert sample_flight.hex == "AB1234"

def test_empty_flight_creation():
    flight = Flight.create_empty()
    assert flight.origin == ""
    assert flight.destination == ""
    assert flight.flight_number == ""
    assert flight.aircraft_type == ""
    assert flight.airline == ""
    assert flight.altitude == 0
    assert flight.vertical_speed == 0
    assert flight.ground_speed == 0

def test_flight_to_json(sample_flight):
    json_str = sample_flight.to_json()
    data = json.loads(json_str)

    assert data["origin"] == "LAX"
    assert data["destination"] == "JFK"
    assert data["flight_number"] == "AA123"
    assert data["aircraft_type"] == "B738"
    assert data["airline"] == "American Airlines"
    assert data["altitude"] == 35000
    assert data["hex"] == "AB1234"
    assert isinstance(data["last_updated"], str)  # Verify datetime serialization

def test_flight_from_json():
    json_data = {
        "origin": "LAX",
        "destination": "JFK",
        "flight_number": "AA123",
        "aircraft_type": "B738",
        "airline": "American Airlines",
        "vertical_speed": 0,
        "altitude": 35000,
        "hex": "AB1234",
        "registration": "N12345",
        "ground_speed": 450,
        "heading": 90,
        "latitude": 34.0522,
        "longitude": -118.2437,
        "last_updated": "2024-01-01T12:00:00Z"
    }

    flight = Flight.from_json(json.dumps(json_data))
    assert flight.origin == "LAX"
    assert flight.destination == "JFK"
    assert flight.flight_number == "AA123"
    assert isinstance(flight.last_updated, datetime)

def test_flight_equality():
    flight1 = Flight(flight_number="AA123", hex="AB1234")
    flight2 = Flight(flight_number="AA123", hex="AB1234")
    flight3 = Flight(flight_number="AA124", hex="AB1234")

    assert flight1 == flight2
    assert flight1 != flight3
    assert flight1 != "not a flight"

def test_flight_validation():
    # Test invalid altitude
    with pytest.raises(ValueError):
        Flight(flight_number="AA123", altitude="invalid")

    # Test invalid coordinates
    with pytest.raises(ValueError):
        Flight(flight_number="AA123", latitude=91)
    with pytest.raises(ValueError):
        Flight(flight_number="AA123", longitude=181)

def test_flight_str_representation(sample_flight):
    str_rep = str(sample_flight)
    assert "AA123" in str_rep
    assert "LAX" in str_rep
    assert "JFK" in str_rep

def test_flight_property_updates(sample_flight):
    # Test updating properties
    sample_flight.altitude = 36000
    assert sample_flight.altitude == 36000

    sample_flight.ground_speed = 460
    assert sample_flight.ground_speed == 460

    # Test invalid updates
    with pytest.raises(ValueError):
        sample_flight.altitude = "invalid"
    with pytest.raises(ValueError):
        sample_flight.latitude = 91
