import pytest
from datetime import datetime
from models.aircraft import Aircraft

@pytest.fixture
def sample_aircraft():
    return Aircraft(
        icao24="abc123",
        callsign="UAL123",
        origin_country="United States",
        time_position=datetime.now(),
        last_contact=datetime.now(),
        longitude=-118.2437,
        latitude=34.0522,
        baro_altitude=35000,
        on_ground=False,
        velocity=450.0,
        true_track=90.0,
        vertical_rate=0,
        sensors=None,
        geo_altitude=35000,
        squawk="1200",
        spi=False,
        position_source=0
    )

def test_aircraft_creation(sample_aircraft):
    assert sample_aircraft.icao24 == "abc123"
    assert sample_aircraft.callsign == "UAL123"
    assert not sample_aircraft.on_ground
    assert sample_aircraft.velocity == 450.0

def test_aircraft_validation():
    with pytest.raises(ValueError):
        Aircraft(icao24="abc123", latitude=91)
    with pytest.raises(ValueError):
        Aircraft(icao24="abc123", longitude=181)
    with pytest.raises(ValueError):
        Aircraft(icao24="abc123", velocity=-1)

def test_aircraft_to_json(sample_aircraft):
    json_data = sample_aircraft.to_json()
    assert isinstance(json_data, str)
    assert "abc123" in json_data
    assert "UAL123" in json_data

def test_aircraft_from_json():
    json_str = '''{"icao24": "abc123", "callsign": "UAL123", "latitude": 34.0522, 
                  "longitude": -118.2437, "velocity": 450.0}'''
    aircraft = Aircraft.from_json(json_str)
    assert aircraft.icao24 == "abc123"
    assert aircraft.callsign == "UAL123"

def test_aircraft_equality():
    aircraft1 = Aircraft(icao24="abc123")
    aircraft2 = Aircraft(icao24="abc123")
    aircraft3 = Aircraft(icao24="def456")

    assert aircraft1 == aircraft2
    assert aircraft1 != aircraft3
