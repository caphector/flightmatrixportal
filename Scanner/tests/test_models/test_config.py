import pytest
import os
from core.config import ConfigHandler

@pytest.fixture
def sample_config():
    return {
        "apis": {
            "opensky": {
                "username": "test_user",
                "password": "test_pass"
            },
            "flightaware": {
                "api_key": "test_key"
            }
        },
        "mqtt": {
            "host": "localhost",
            "port": 1883,
            "topic": "aircraft/tracking"
        },
        "tracking": {
            "update_interval": 30,
            "max_distance": 100
        }
    }

def test_config_loading(tmp_path, sample_config):
    config_path = tmp_path / "config.yaml"
    ConfigHandler.save_config(sample_config, config_path)

    loaded_config = ConfigHandler.load_config(config_path)
    assert loaded_config["apis"]["opensky"]["username"] == "test_user"
    assert loaded_config["mqtt"]["port"] == 1883

def test_config_validation():
    invalid_config = {"apis": {}}
    with pytest.raises(ValueError):
        ConfigHandler.validate_config(invalid_config)

def test_config_environment_override(monkeypatch):
    monkeypatch.setenv("OPENSKY_USERNAME", "env_user")
    config = ConfigHandler.load_config_with_env()
    assert config["apis"]["opensky"]["username"] == "env_user"
