import pytest
from unittest.mock import Mock, patch
from api.flightaware_api import FlightAwareAPI
from models.flight import Flight

@pytest.fixture
def flightaware_api():
    limiter = Mock()
    limiter.can_make_request.return_value = True
    return FlightAwareAPI(
        api_key='test_key',
        limiter=limiter
    )

@patch('requests.get')
def test_get_flight_info_success(mock_get, flightaware_api):
    # Mock successful API response
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        "flights": [{
            "ident": "UAL123",
            "origin": {
                "code": "LAX",
                "name": "Los Angeles Intl",
                "city": "Los Angeles"
            },
            "destination": {
                "code": "JFK",
                "name": "John F Kennedy Intl",
                "city": "New York"
            },
            "airline": {
                "name": "United Airlines"
            },
            "aircraft_type": "B738",
            "filed_altitude": 35000,
            "route": "DIRECT",
            "status": "Scheduled"
        }]
    }

    result = flightaware_api.get_flight_info("UAL123")

    assert isinstance(result, Flight)
    assert result.flight_number == "UAL123"
    assert result.origin == "LAX"
    assert result.destination == "JFK"
    assert result.aircraft_type == "B738"
    assert result.airline == "United Airlines"

@patch('requests.get')
def test_get_flight_info_no_flights(mock_get, flightaware_api):
    # Mock empty response
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        "flights": []
    }

    result = flightaware_api.get_flight_info("UAL123")
    assert result is None

@patch('requests.get')
def test_get_flight_info_error(mock_get, flightaware_api):
    # Mock API error
    mock_get.return_value.status_code = 401
    mock_get.return_value.json.side_effect = ValueError

    result = flightaware_api.get_flight_info("UAL123")
    assert result is None

def test_get_flight_info_rate_limited(flightaware_api):
    # Test rate limiting
    flightaware_api.limiter.can_make_request.return_value = False
    result = flightaware_api.get_flight_info("UAL123")
    assert result is None

@patch('requests.get')
def test_get_airport_info_success(mock_get, flightaware_api):
    # Mock successful airport info response
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        "name": "John F Kennedy Intl",
        "latitude": 40.6413,
        "longitude": -73.7781,
        "elevation": 13,
        "city": "New York",
        "country_code": "US",
        "timezone": "America/New_York"
    }

    result = flightaware_api.get_airport_info("JFK")

    assert result is not None
    assert result["name"] == "John F Kennedy Intl"
    assert result["latitude"] == 40.6413
    assert result["longitude"] == -73.7781

@patch('requests.get')
def test_get_airport_info_error(mock_get, flightaware_api):
    # Mock API error for airport info
    mock_get.return_value.status_code = 404

    result = flightaware_api.get_airport_info("INVALID")
    assert result is None

@patch('requests.get')
def test_get_flight_track_success(mock_get, flightaware_api):
    # Mock successful flight track response
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        "positions": [
            {
                "latitude": 40.6413,
                "longitude": -73.7781,
                "altitude": 35000,
                "groundspeed": 450,
                "heading": 90,
                "timestamp": "2024-01-01T12:00:00Z"
            },
            {
                "latitude": 40.6415,
                "longitude": -73.7785,
                "altitude": 35100,
                "groundspeed": 451,
                "heading": 91,
                "timestamp": "2024-01-01T12:01:00Z"
            }
        ]
    }

    result = flightaware_api.get_flight_track("UAL123")

    assert len(result) == 2
    assert "latitude" in result[0]
    assert "longitude" in result[0]
    assert "altitude" in result[0]
    assert result[0]["altitude"] == 35000

@patch('requests.get')
def test_get_flight_track_error(mock_get, flightaware_api):
    # Mock API error for flight track
    mock_get.return_value.status_code = 404

    result = flightaware_api.get_flight_track("INVALID")
    assert result == []
