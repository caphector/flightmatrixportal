# tests/test_api/test_opensky_api.py
import pytest
from unittest.mock import Mock, patch
from datetime import datetime, timezone
from api.opensky_api import OpenSkyAPI
from models.aircraft import Aircraft

@pytest.fixture
def opensky_api():
    limiter = Mock()
    limiter.can_make_request.return_value = True
    return OpenSkyAPI(
        username='test_user',
        password='test_pass',
        limiter=limiter
    )

@patch('requests.get')
def test_get_states_success(mock_get, opensky_api):
    # Mock successful API response
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        "time": 1622544000,
        "states": [[
            "a1b2c3",  # icao24
            "UAL123",  # callsign
            "USA",     # origin_country
            1622544000,# time_position
            1622544000,# last_contact
            -73.778,   # longitude
            40.639,    # latitude
            10000,     # baro_altitude
            False,     # on_ground
            250,       # velocity
            90,        # true_track
            5,         # vertical_rate
            None,      # sensors
            11000,     # geo_altitude
            "1000",    # squawk
            False,     # spi
            0         # position_source
        ]]
    }

    result = opensky_api.get_states()

    assert len(result) == 1
    aircraft = result[0]
    assert isinstance(aircraft, Aircraft)
    assert aircraft.icao24 == "a1b2c3"
    assert aircraft.callsign == "UAL123"
    assert aircraft.latitude == 40.639
    assert aircraft.longitude == -73.778
    assert aircraft.altitude == 10000
    assert aircraft.velocity == 250
    assert aircraft.vertical_rate == 5

@patch('requests.get')
def test_get_states_empty(mock_get, opensky_api):
    # Mock empty response
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        "time": 1622544000,
        "states": []
    }

    result = opensky_api.get_states()
    assert result == []

@patch('requests.get')
def test_get_states_error(mock_get, opensky_api):
    # Mock API error
    mock_get.return_value.status_code = 403
    mock_get.return_value.json.side_effect = ValueError

    result = opensky_api.get_states()
    assert result == []

def test_get_states_rate_limited(opensky_api):
    # Test rate limiting
    opensky_api.limiter.can_make_request.return_value = False
    result = opensky_api.get_states()
    assert result == []

@patch('requests.get')
def test_get_track_success(mock_get, opensky_api):
    # Mock successful track history
    mock_time = int(datetime.now(timezone.utc).timestamp())
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = [
        [mock_time, -73.778, 40.639, 10000, False, 250, 90, 5],
        [mock_time - 10, -73.780, 40.640, 10100, False, 251, 91, 6]
    ]

    result = opensky_api.get_track("a1b2c3")

    assert len(result) == 2
    assert all(len(point) == 8 for point in result)
    assert result[0][3] == 10000  # altitude
    assert result[1][3] == 10100  # altitude

@patch('requests.get')
def test_get_track_error(mock_get, opensky_api):
    # Mock API error for track
    mock_get.return_value.status_code = 404

    result = opensky_api.get_track("a1b2c3")
    assert result == []
