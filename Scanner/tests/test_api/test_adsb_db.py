import pytest
from unittest.mock import Mock, patch
from datetime import datetime, timezone
from api.adsb_db import ADSBdbAPI
from models.flight import Flight
from models.aircraft import Aircraft

@pytest.fixture
def adsb_api():
    limiter = Mock()
    limiter.can_make_request.return_value = True
    return ADSBdbAPI(
        base_url='http://test.com/',
        api_key='test_key',
        limiter=limiter,
        airlines_file='tests/data/airlines.csv'
    )

@patch('requests.get')
def test_get_flight_data_success(mock_get, adsb_api):
    # Mock successful API response
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        'response': {
            'flightroute': {
                'airline': {'name': 'Test Airline', 'icao': 'TST'},
                'origin': {
                    'iata_code': 'LAX',
                    'name': 'Los Angeles International',
                    'latitude': 33.9425,
                    'longitude': -118.408
                },
                'destination': {
                    'iata_code': 'JFK',
                    'name': 'John F Kennedy International',
                    'latitude': 40.6398,
                    'longitude': -73.7789
                },
                'aircraft': {
                    'model': 'Boeing 737-800',
                    'type': 'B738'
                }
            }
        }
    }

    result = adsb_api.get_flight_data({
        'flight': 'TST123',
        'hex': 'ABCDEF',
        'baro_rate': 0,
        'latitude': 34.0,
        'longitude': -118.0,
        'altitude': 30000
    })

    assert isinstance(result, Flight)
    assert result.flight_number == 'TST123'
    assert result.origin == 'LAX'
    assert result.destination == 'JFK'
    assert result.airline == 'Test Airline'
    assert result.aircraft_type == 'B738'

@patch('requests.get')
def test_get_flight_data_minimal(mock_get, adsb_api):
    # Test with minimal response data
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        'response': {
            'flightroute': {
                'airline': {'name': 'Test Airline'},
                'origin': {'iata_code': 'LAX'},
                'destination': {'iata_code': 'JFK'}
            }
        }
    }

    result = adsb_api.get_flight_data({
        'flight': 'TST123',
        'hex': 'ABCDEF',
        'baro_rate': 0
    })

    assert isinstance(result, Flight)
    assert result.origin == 'LAX'
    assert result.destination == 'JFK'
    assert result.airline == 'Test Airline'

def test_get_flight_data_missing_info(adsb_api):
    # Test with missing required data
    result = adsb_api.get_flight_data({})
    assert result is None

    result = adsb_api.get_flight_data({'flight': 'TST123'})  # Missing hex
    assert result is None

@patch('requests.get')
def test_get_flight_data_api_error(mock_get, adsb_api):
    # Test API error handling
    mock_get.return_value.status_code = 404
    mock_get.return_value.json.side_effect = ValueError

    result = adsb_api.get_flight_data({
        'flight': 'TST123',
        'hex': 'ABCDEF',
        'baro_rate': 0
    })
    assert result is None

def test_get_flight_data_rate_limited(adsb_api):
    # Test rate limiting behavior
    adsb_api.limiter.can_make_request.return_value = False

    result = adsb_api.get_flight_data({
        'flight': 'TST123',
        'hex': 'ABCDEF',
        'baro_rate': 0
    })
    assert result is None

@patch('requests.get')
def test_get_aircraft_info_success(mock_get, adsb_api):
    # Test successful aircraft info retrieval
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        'response': {
            'aircraft': {
                'hex': 'ABCDEF',
                'registration': 'N12345',
                'manufacturer': 'Boeing',
                'model': '737-800',
                'type': 'B738',
                'owner': 'Test Airlines',
                'year_built': 2015
            }
        }
    }

    result = adsb_api.get_aircraft_info('ABCDEF')

    assert isinstance(result, dict)
    assert result['registration'] == 'N12345'
    assert result['type'] == 'B738'
    assert result['year_built'] == 2015

@patch('requests.get')
def test_get_aircraft_info_error(mock_get, adsb_api):
    # Test aircraft info error handling
    mock_get.return_value.status_code = 404

    result = adsb_api.get_aircraft_info('INVALID')
    assert result is None

@patch('requests.get')
def test_get_airport_info_success(mock_get, adsb_api):
    # Test successful airport info retrieval
    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        'response': {
            'airport': {
                'iata': 'LAX',
                'name': 'Los Angeles International',
                'latitude': 33.9425,
                'longitude': -118.408,
                'elevation': 125,
                'country': 'United States',
                'timezone': 'America/Los_Angeles'
            }
        }
    }

    result = adsb_api.get_airport_info('LAX')

    assert isinstance(result, dict)
    assert result['iata'] == 'LAX'
    assert result['latitude'] == 33.9425
    assert result['longitude'] == -118.408

@patch('requests.get')
def test_get_airport_info_error(mock_get, adsb_api):
    # Test airport info error handling
    mock_get.return_value.status_code = 404

    result = adsb_api.get_airport_info('INVALID')
    assert result is None

@patch('requests.get')
def test_process_raw_data_success(mock_get, adsb_api):
    # Test processing of raw aircraft data
    raw_data = {
        'hex': 'ABCDEF',
        'flight': 'TST123',
        'lat': 33.9425,
        'lon': -118.408,
        'altitude': 30000,
        'track': 90,
        'speed': 450,
        'vert_rate': 0,
        'squawk': '1200'
    }

    mock_get.return_value.status_code = 200
    mock_get.return_value.json.return_value = {
        'response': {
            'aircraft': {
                'type': 'B738',
                'registration': 'N12345'
            }
        }
    }

    result = adsb_api.process_raw_data(raw_data)

    assert isinstance(result, Aircraft)
    assert result.icao24 == 'ABCDEF'
    assert result.callsign == 'TST123'
    assert result.latitude == 33.9425
    assert result.longitude == -118.408
    assert result.altitude == 30000
    assert result.heading == 90
    assert result.velocity == 450
    assert result.vertical_rate == 0

@patch('requests.get')
def test_process_raw_data_minimal(mock_get, adsb_api):
    # Test processing with minimal raw data
    raw_data = {
        'hex': 'ABCDEF',
        'lat': 33.9425,
        'lon': -118.408
    }

    result = adsb_api.process_raw_data(raw_data)

    assert isinstance(result, Aircraft)
    assert result.icao24 == 'ABCDEF'
    assert result.latitude == 33.9425
    assert result.longitude == -118.408

def test_process_raw_data_invalid():
    # Test processing invalid raw data
    adsb_api = ADSBdbAPI(base_url='http://test.com/', api_key='test_key')

    result = adsb_api.process_raw_data({})
    assert result is None

    result = adsb_api.process_raw_data({'hex': 'ABCDEF'})  # Missing required lat/lon
    assert result is None
