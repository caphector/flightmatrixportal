import pytest
import logging
from utils.logger import setup_logger

def test_logger_setup(tmp_path):
    log_file = tmp_path / "test.log"
    logger = setup_logger("test_logger", log_file)

    assert isinstance(logger, logging.Logger)
    assert logger.level == logging.INFO

    logger.info("Test message")
    assert log_file.exists()
    assert "Test message" in log_file.read_text()

def test_logger_formatting(tmp_path):
    log_file = tmp_path / "test.log"
    logger = setup_logger("test_logger", log_file)

    logger.error("Error message")
    log_content = log_file.read_text()
    assert "ERROR" in log_content
    assert "Error message" in log_content
