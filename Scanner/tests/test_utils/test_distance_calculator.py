import pytest
from utils.distance_calculator import calculate_distance

@pytest.fixture
def sample_coordinates():
    return {
        "los_angeles": (34.0522, -118.2437),
        "new_york": (40.7128, -74.0060),
        "london": (51.5074, -0.1278),
        "tokyo": (35.6762, 139.6503),
        "sydney": (-33.8688, 151.2093)
    }

def test_calculate_distance_tuple_inputs(sample_coordinates):
    # Test known distances between major cities
    distance = calculate_distance(sample_coordinates["los_angeles"], sample_coordinates["new_york"])
    assert 2400 < distance < 2500  # ~2450 miles

    distance = calculate_distance(sample_coordinates["new_york"], sample_coordinates["london"])
    assert 3400 < distance < 3500  # ~3460 miles

    distance = calculate_distance(sample_coordinates["tokyo"], sample_coordinates["sydney"])
    assert 4800 < distance < 4900  # ~4850 miles

def test_calculate_distance_dict_inputs():
    home_coords = (34.0522, -118.2437)
    flight_coords = {
        "lat": 40.7128,
        "lon": -74.0060,
        "latitude": 40.7128,  # Alternative keys
        "longitude": -74.0060
    }

    # Test with lat/lon keys
    distance1 = calculate_distance(flight_coords, home_coords)
    assert 2400 < distance1 < 2500

    # Test with latitude/longitude keys
    flight_coords.pop("lat")
    flight_coords.pop("lon")
    distance2 = calculate_distance(flight_coords, home_coords)
    assert 2400 < distance2 < 2500

def test_calculate_distance_same_point(sample_coordinates):
    # Distance to self should be 0
    la_coords = sample_coordinates["los_angeles"]
    distance = calculate_distance(la_coords, la_coords)
    assert distance == 0

def test_calculate_distance_invalid_data():
    home_coords = (34.0522, -118.2437)

    # Test various invalid inputs
    invalid_inputs = [
        {"invalid": "data"},
        {},
        {"lat": "invalid", "lon": -74.0060},
        {"lat": 40.7128},  # Missing longitude
        None,
        "invalid string",
        (91, 0),  # Invalid latitude
        (0, 181)  # Invalid longitude
    ]

    for invalid_input in invalid_inputs:
        distance = calculate_distance(invalid_input, home_coords)
        assert distance == float('inf')

def test_calculate_distance_edge_cases():
    # Test poles
    north_pole = (90, 0)
    south_pole = (-90, 0)
    equator_point = (0, 0)

    distance = calculate_distance(north_pole, south_pole)
    assert 12400 < distance < 12500  # ~12450 miles (half Earth's circumference)

    distance = calculate_distance(north_pole, equator_point)
    assert 6200 < distance < 6300  # ~6225 miles (quarter Earth's circumference)

def test_calculate_distance_international_dateline():
    # Test points across international dateline
    point_west = (0, 179)
    point_east = (0, -179)

    distance = calculate_distance(point_west, point_east)
    assert distance < 250  # Should be small despite longitude difference
