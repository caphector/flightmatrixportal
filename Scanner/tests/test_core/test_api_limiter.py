import pytest
from datetime import datetime, timedelta
from core.api_limiter import APILimiter, TokenBucket

@pytest.fixture
def token_bucket():
    return TokenBucket(
        max_tokens=100,
        tokens_remaining=100,
        last_updated=datetime.now(),
        reset_date=datetime.now() + timedelta(days=30),
        safety_margin=5,
        daily_quota=10
    )

@pytest.fixture
def api_limiter():
    return APILimiter(monthly_limit=1000, name="test_api")

def test_token_bucket_creation(token_bucket):
    assert token_bucket.max_tokens == 100
    assert token_bucket.tokens_remaining == 100
    assert token_bucket.safety_margin == 5
    assert token_bucket.daily_quota == 10
    assert isinstance(token_bucket.last_updated, datetime)
    assert isinstance(token_bucket.reset_date, datetime)

def test_token_bucket_reset():
    # Test bucket reset after expiration
    past_date = datetime.now() - timedelta(days=31)
    bucket = TokenBucket(
        max_tokens=100,
        tokens_remaining=0,
        last_updated=past_date,
        reset_date=past_date + timedelta(days=30),
        safety_margin=5,
        daily_quota=10
    )

    bucket.update()
    assert bucket.tokens_remaining == 100
    assert bucket.reset_date > datetime.now()

def test_token_bucket_partial_refill():
    # Test partial token refill based on time passed
    past_date = datetime.now() - timedelta(hours=12)
    bucket = TokenBucket(
        max_tokens=100,
        tokens_remaining=0,
        last_updated=past_date,
        reset_date=datetime.now() + timedelta(days=15),
        safety_margin=5,
        daily_quota=10
    )

    bucket.update()
    assert 0 < bucket.tokens_remaining < 100

def test_api_limiter_init(api_limiter):
    assert api_limiter.name == "test_api"
    assert api_limiter.bucket.max_tokens == 1000
    assert api_limiter.bucket.tokens_remaining == 1000

def test_api_limiter_can_make_request(api_limiter):
    # Test normal request
    assert api_limiter.can_make_request(cost=1) == True

    # Test request exceeding remaining tokens
    api_limiter.bucket.tokens_remaining = 5
    assert api_limiter.can_make_request(cost=10) == False

    # Test request at safety margin
    assert api_limiter.can_make_request(cost=5) == False

def test_api_limiter_consume_tokens(api_limiter):
    initial_tokens = api_limiter.bucket.tokens_remaining

    # Test successful consumption
    assert api_limiter.consume_tokens(cost=1) == True
    assert api_limiter.bucket.tokens_remaining == initial_tokens - 1

    # Test failed consumption
    api_limiter.bucket.tokens_remaining = 5
    assert api_limiter.consume_tokens(cost=10) == False
    assert api_limiter.bucket.tokens_remaining == 5  # Should not change

def test_api_limiter_daily_quota():
    limiter = APILimiter(
        monthly_limit=1000,
        name="test_api",
        daily_quota=5
    )

    # Test daily quota enforcement
    for _ in range(5):
        assert limiter.consume_tokens(cost=1) == True

    # Should fail after reaching daily quota
    assert limiter.consume_tokens(cost=1) == False

def test_api_limiter_zero_cost():
    limiter = APILimiter(monthly_limit=10, name="test_api")

    # Test zero cost requests
    assert limiter.can_make_request(cost=0) == True
    assert limiter.consume_tokens(cost=0) == True
    assert limiter.bucket.tokens_remaining == 10  # Should not change

def test_api_limiter_negative_cost():
    limiter = APILimiter(monthly_limit=10, name="test_api")

    # Test invalid negative cost
    with pytest.raises(ValueError):
        limiter.can_make_request(cost=-1)

    with pytest.raises(ValueError):
        limiter.consume_tokens(cost=-1)

def test_api_limiter_reset():
    past_date = datetime.now() - timedelta(days=31)
    limiter = APILimiter(
        monthly_limit=100,
        name="test_api",
        initial_tokens=0,
        reset_date=past_date + timedelta(days=30)
    )

    # Force a reset by checking availability
    assert limiter.can_make_request(cost=1) == True
    assert limiter.bucket.tokens_remaining == 100

def test_api_limiter_safety_margin():
    limiter = APILimiter(
        monthly_limit=100,
        name="test_api",
        safety_margin=20
    )

    # Reduce tokens to near safety margin
    limiter.bucket.tokens_remaining = 25

    # Should allow requests above safety margin
    assert limiter.can_make_request(cost=4) == True

    # Should deny requests that would go below safety margin
    assert limiter.can_make_request(cost=6) == False

def test_api_limiter_concurrent_usage():
    limiter = APILimiter(monthly_limit=10, name="test_api")

    # Simulate concurrent requests
    success_count = 0
    for _ in range(10):
        if limiter.can_make_request(cost=1) and limiter.consume_tokens(cost=1):
            success_count += 1

    assert success_count == 10  # All should succeed
    assert limiter.bucket.tokens_remaining == 0  # Should be depleted
