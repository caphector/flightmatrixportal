import pytest
from unittest.mock import Mock, patch
from core.app import AircraftTracker

@pytest.fixture
def tracker():
    config = {
        "apis": {
            "opensky": {"username": "test", "password": "test"},
            "flightaware": {"api_key": "test"}
        },
        "mqtt": {
            "host": "localhost",
            "port": 1883
        }
    }
    return AircraftTracker(config)

@patch('core.app.OpenSkyAPI')
@patch('core.app.FlightAwareAPI')
@patch('core.app.MQTTHandler')
def test_tracker_initialization(mock_mqtt, mock_flightaware, mock_opensky, tracker):
    assert tracker.config is not None
    mock_opensky.assert_called_once()
    mock_flightaware.assert_called_once()
    mock_mqtt.assert_called_once()

@patch('core.app.OpenSkyAPI')
def test_tracker_update_cycle(mock_opensky, tracker):
    mock_opensky.return_value.get_states.return_value = [
        Mock(icao24="abc123", latitude=34.0522, longitude=-118.2437)
    ]

    tracker.update()
    assert len(tracker.tracked_aircraft) == 1
