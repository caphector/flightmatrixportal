import pytest
from unittest.mock import Mock, patch
from core.mqtt_handler import MQTTHandler

@pytest.fixture
def mqtt_handler():
    with patch('paho.mqtt.client.Client') as mock_client:
        handler = MQTTHandler(
            host='localhost',
            port=1883,
            user='test',
            password='test',
            topic='test/topic',
            messages_per_second=2.0
        )
        yield handler

def test_mqtt_publish(mqtt_handler):
    # Test successful publish
    mqtt_handler.client.publish.return_value = (0, 0)
    mqtt_handler.publish('test message')
    mqtt_handler.client.publish.assert_called_once()

def test_mqtt_publish_failure(mqtt_handler):
    # Test failed publish
    mqtt_handler.client.publish.return_value = (1, 0)
    mqtt_handler.publish('test message')
    mqtt_handler.client.publish.assert_called_once()
