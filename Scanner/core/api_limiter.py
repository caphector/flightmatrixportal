# core/api_limiter.py
from datetime import datetime, timedelta
import json
from pathlib import Path
import logging
from dataclasses import dataclass

@dataclass
class TokenBucket:
    max_tokens: int
    tokens_remaining: int
    last_updated: datetime
    reset_date: datetime
    safety_margin: int = 0  # Keep some tokens in reserve
    daily_quota: int = 0    # Daily token quota for distribution

    def to_dict(self):
        return {
            "max_tokens": self.max_tokens,
            "tokens_remaining": self.tokens_remaining,
            "last_updated": self.last_updated.isoformat(),
            "reset_date": self.reset_date.isoformat(),
            "safety_margin": self.safety_margin,
            "daily_quota": self.daily_quota
        }

    @classmethod
    def from_dict(cls, data):
        return cls(
            max_tokens=data["max_tokens"],
            tokens_remaining=data["tokens_remaining"],
            last_updated=datetime.fromisoformat(data["last_updated"]),
            reset_date=datetime.fromisoformat(data["reset_date"]),
            safety_margin=data["safety_margin"],
            daily_quota=data["daily_quota"]
        )

class APILimiter:
    def __init__(self, monthly_limit: int, name: str, safety_percentage: float = 0.05):
        self.name = name
        self.state_file = Path(f"api_limiter_{name.lower()}.json")
        self.safety_margin = int(monthly_limit * safety_percentage)
        self.bucket = self._load_or_init_bucket(monthly_limit)

    def _load_or_init_bucket(self, monthly_limit: int) -> TokenBucket:
        """Load saved state or initialize new bucket"""
        if self.state_file.exists():
            try:
                with open(self.state_file, 'r') as f:
                    data = json.load(f)
                bucket = TokenBucket.from_dict(data)

                # Reset if we're in a new month
                if datetime.now() >= bucket.reset_date:
                    return self._init_bucket(monthly_limit)

                return bucket
            except Exception as e:
                logging.error(f"Error loading state for {self.name}: {e}")
                return self._init_bucket(monthly_limit)
        return self._init_bucket(monthly_limit)

    def _init_bucket(self, monthly_limit: int) -> TokenBucket:
        """Initialize new token bucket with monthly distribution"""
        now = datetime.now()
        reset_date = (now.replace(day=1) + timedelta(days=32)).replace(day=1)
        days_in_month = (reset_date - now).days
        daily_quota = (monthly_limit - self.safety_margin) // days_in_month

        return TokenBucket(
            max_tokens=monthly_limit,
            tokens_remaining=monthly_limit,
            last_updated=now,
            reset_date=reset_date,
            safety_margin=self.safety_margin,
            daily_quota=daily_quota
        )

    def _save_state(self):
        """Persist current state to file"""
        try:
            with open(self.state_file, 'w') as f:
                json.dump(self.bucket.to_dict(), f)
        except Exception as e:
            logging.error(f"Error saving state for {self.name}: {e}")

    def can_make_request(self, cost: int = 1, priority: bool = False) -> bool:
        """Check if we have enough tokens, considering priority"""
        now = datetime.now()

        # Reset bucket if we're in a new month
        if now >= self.bucket.reset_date:
            self.bucket = self._init_bucket(self.bucket.max_tokens)
            self._save_state()

        available_tokens = self.bucket.tokens_remaining
        if not priority:
            # For non-priority requests, respect daily quota and safety margin
            available_tokens = min(
                available_tokens - self.bucket.safety_margin,
                self.bucket.daily_quota
            )

        return available_tokens >= cost

    def consume_tokens(self, cost: int = 1, priority: bool = False) -> bool:
        """Consume tokens if available"""
        if not self.can_make_request(cost, priority):
            return False

        self.bucket.tokens_remaining -= cost
        self.bucket.last_updated = datetime.now()
        self._save_state()
        return True

    def get_bucket_stats(self):
        """Get detailed statistics about the bucket's state"""
        now = datetime.now()
        return {
            "name": self.name,
            "tokens_remaining": self.bucket.tokens_remaining,
            "max_tokens": self.bucket.max_tokens,
            "safety_margin": self.bucket.safety_margin,
            "daily_quota": self.bucket.daily_quota,
            "days_until_reset": (self.bucket.reset_date - now).days,
            "last_updated": self.bucket.last_updated.isoformat(),
            "reset_date": self.bucket.reset_date.isoformat(),
            "percent_remaining": (self.bucket.tokens_remaining / self.bucket.max_tokens) * 100
        }

    def get_status(self) -> dict:
        """Get current status including quotas"""
        return {
            "tokens_remaining": self.bucket.tokens_remaining,
            "max_tokens": self.bucket.max_tokens,
            "reset_date": self.bucket.reset_date.isoformat(),
            "last_updated": self.bucket.last_updated.isoformat(),
            "safety_margin": self.bucket.safety_margin,
            "daily_quota": self.bucket.daily_quota
        }