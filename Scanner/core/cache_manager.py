import json
import logging
from typing import Optional, Dict, Any, Tuple
from datetime import datetime, timedelta
from pathlib import Path
from functools import lru_cache
from threading import Lock

class CacheManager:
    def __init__(
        self,
        cache_duration: timedelta = timedelta(minutes=15),
        max_memory_entries: int = 1000,
        cache_dir: str = "cache"
    ):
        self.cache_duration = cache_duration
        self.memory_cache: Dict[str, Tuple[datetime, Any]] = {}
        self.max_memory_entries = max_memory_entries
        self.cache_dir = Path(cache_dir)
        self.cache_dir.mkdir(exist_ok=True)
        self.lock = Lock()

    def _generate_cache_key(self, flight_id: str, hex_id: str) -> str:
        """Generate a standardized cache key"""
        timestamp = datetime.now().replace(
            minute=datetime.now().minute - (datetime.now().minute % 15),
            second=0,
            microsecond=0
        )
        return f"{flight_id}_{hex_id}_{timestamp.isoformat()}"

    def _is_cache_valid(self, cached_time: datetime) -> bool:
        """Check if cached data is still valid"""
        return datetime.now() - cached_time < self.cache_duration

    @lru_cache(maxsize=128)
    def get_file_path(self, cache_key: str) -> Path:
        """Get the file path for a cache key"""
        return self.cache_dir / f"{cache_key}.json"

    def get(self, flight_id: str, hex_id: str) -> Optional[Dict]:
        """
        Get flight data from cache
        First checks memory cache, then file cache
        """
        cache_key = self._generate_cache_key(flight_id, hex_id)

        # Check memory cache first
        with self.lock:
            if cache_key in self.memory_cache:
                cached_time, cached_data = self.memory_cache[cache_key]
                if self._is_cache_valid(cached_time):
                    logging.debug(f"Memory cache hit for {cache_key}")
                    return cached_data
                else:
                    del self.memory_cache[cache_key]

        # Check file cache if not in memory
        try:
            file_path = self.get_file_path(cache_key)
            if file_path.exists():
                with file_path.open('r') as f:
                    cached_data = json.load(f)
                    cached_time = datetime.fromisoformat(cached_data['timestamp'])

                    if self._is_cache_valid(cached_time):
                        # Add to memory cache for faster subsequent access
                        with self.lock:
                            self.memory_cache[cache_key] = (cached_time, cached_data['data'])
                        logging.debug(f"File cache hit for {cache_key}")
                        return cached_data['data']
                    else:
                        file_path.unlink()  # Remove expired cache file

        except Exception as e:
            logging.error(f"Cache read error for {cache_key}: {str(e)}")

        return None

    def set(self, flight_id: str, hex_id: str, data: Dict) -> None:
        """
        Store flight data in both memory and file cache
        """
        cache_key = self._generate_cache_key(flight_id, hex_id)
        current_time = datetime.now()

        # Update memory cache
        with self.lock:
            # Clean up memory cache if it's too large
            if len(self.memory_cache) >= self.max_memory_entries:
                self._cleanup_memory_cache()

            self.memory_cache[cache_key] = (current_time, data)

        # Update file cache
        try:
            cache_data = {
                'timestamp': current_time.isoformat(),
                'data': data
            }

            file_path = self.get_file_path(cache_key)
            with file_path.open('w') as f:
                json.dump(cache_data, f)

            logging.debug(f"Cached data for {cache_key}")
        except Exception as e:
            logging.error(f"Cache write error for {cache_key}: {str(e)}")

    def _cleanup_memory_cache(self) -> None:
        """Remove expired and excess entries from memory cache"""
        with self.lock:
            # Remove expired entries
            current_time = datetime.now()
            expired_keys = [
                key for key, (cached_time, _) in self.memory_cache.items()
                if not self._is_cache_valid(cached_time)
            ]
            for key in expired_keys:
                del self.memory_cache[key]

            # If still too many entries, remove oldest
            if len(self.memory_cache) >= self.max_memory_entries:
                sorted_entries = sorted(
                    self.memory_cache.items(),
                    key=lambda x: x[1][0]  # Sort by timestamp
                )
                entries_to_remove = len(self.memory_cache) - self.max_memory_entries + 100  # Remove extra to prevent frequent cleanups
                for key, _ in sorted_entries[:entries_to_remove]:
                    del self.memory_cache[key]

    def cleanup_expired_files(self) -> None:
        """Remove expired cache files"""
        try:
            current_time = datetime.now()
            for cache_file in self.cache_dir.glob("*.json"):
                try:
                    with cache_file.open('r') as f:
                        cached_data = json.load(f)
                        cached_time = datetime.fromisoformat(cached_data['timestamp'])

                        if not self._is_cache_valid(cached_time):
                            cache_file.unlink()
                            logging.debug(f"Removed expired cache file: {cache_file.name}")
                except Exception as e:
                    logging.error(f"Error processing cache file {cache_file}: {str(e)}")
                    cache_file.unlink()  # Remove corrupted cache files

        except Exception as e:
            logging.error(f"Cache cleanup error: {str(e)}")

    def clear(self) -> None:
        """Clear all cache data"""
        with self.lock:
            self.memory_cache.clear()

        try:
            for cache_file in self.cache_dir.glob("*.json"):
                cache_file.unlink()
            logging.info("Cache cleared")
        except Exception as e:
            logging.error(f"Cache clear error: {str(e)}")

    def get_stats(self) -> Dict:
        """Get cache statistics"""
        try:
            file_count = len(list(self.cache_dir.glob("*.json")))
            memory_count = len(self.memory_cache)

            total_size = sum(
                cache_file.stat().st_size
                for cache_file in self.cache_dir.glob("*.json")
            )

            return {
                "memory_entries": memory_count,
                "file_entries": file_count,
                "total_size_bytes": total_size,
                "cache_duration_minutes": self.cache_duration.total_seconds() / 60
            }
        except Exception as e:
            logging.error(f"Error getting cache stats: {str(e)}")
            return {}
