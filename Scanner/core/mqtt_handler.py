from time import time, sleep
from typing import Optional
import logging
import random
from paho.mqtt import client as mqtt_client

class MQTTRateLimiter:
    def __init__(self, messages_per_second: float):
        self.min_interval = 1.0 / messages_per_second
        self.last_send_time: Optional[float] = None

    def wait_if_needed(self):
        if self.last_send_time is None:
            self.last_send_time = time()
            return

        elapsed = time() - self.last_send_time
        if elapsed < self.min_interval:
            sleep(self.min_interval - elapsed)

        self.last_send_time = time()

class MQTTHandler:
    def __init__(
        self, 
        host: str, 
        port: int, 
        user: str, 
        password: str, 
        topic: str, 
        messages_per_second: float = 2.0
    ):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.topic = topic
        self.client_id = f'python-mqtt-{random.randint(0, 1000)}'
        self.rate_limiter = MQTTRateLimiter(messages_per_second)
        self.client = self._connect()

    def _connect(self):
        def on_connect(client, userdata, flags, rc):
            client.subscribe(self.topic)
            if rc == 0:
                pass
            else:
                logging.info(f"Failed to connect, return code {rc}\n")

        client = mqtt_client.Client(self.client_id)
        client.username_pw_set(self.user, self.password)
        client.on_connect = on_connect
        client.connect(self.host, self.port)
        return client

    def publish(self, message, retain=False):
        self.rate_limiter.wait_if_needed()
        self.client.reconnect()
        result = self.client.publish(self.topic, message, qos=0, retain=retain)
        if result[0] == 0:
            if not message.startswith('{"origin": "", "destination": ""'):
                logging.info(f"Sent `{message}` to topic `{self.topic}`")
        else:
            logging.info(f"Failed to send message to topic {self.topic}")


# import random
# import logging
# from time import time, sleep
# from typing import Optional
# from paho.mqtt import client as mqtt_client

# class MQTTHandler:
#     def __init__(self, host, port, user, password, topic):
#         self.host = host
#         self.port = port
#         self.user = user
#         self.password = password
#         self.topic = topic
#         self.rate_limiter = MQTTRateLimiter(messages_per_second)
#         self.client_id = f'python-mqtt-{random.randint(0, 1000)}'
#         self.client = self._connect()

#     def publish(self, message, retain=False):
#         self.rate_limiter.wait_if_needed()
#         self.client.reconnect()
#         result = self.client.publish(self.topic, message, qos=0, retain=retain)
#         if result[0] == 0:
#             if not message.startswith('{"origin": "", "destination": ""'):
#                 logging.info(f"Sent `{message}` to topic `{self.topic}`")
#         else:
#             logging.info(f"Failed to send message to topic {self.topic}")

#     def _connect(self):
#         def on_connect(client, userdata, flags, rc):
#             client.subscribe(self.topic)
#             if rc == 0:
#                 pass
#             else:
#                 logging.info(f"Failed to connect, return code {rc}\n")

#         client = mqtt_client.Client(self.client_id)
#         client.username_pw_set(self.user, self.password)
#         client.on_connect = on_connect
#         client.connect(self.host, self.port)
#         return client

#     def publish(self, message, retain=False):
#         self.client.reconnect()
#         result = self.client.publish(self.topic, message, qos=0, retain=retain)
#         if result[0] == 0:
#             if not message.startswith('{"origin": "", "destination": ""'):
#                 logging.info(f"Sent `{message}` to topic `{self.topic}`")
#         else:
#             logging.info(f"Failed to send message to topic {self.topic}")

# class MQTTRateLimiter:
#     def __init__(self, messages_per_second: float):
#         self.min_interval = 1.0 / messages_per_second
#         self.last_send_time: Optional[float] = None

#     def wait_if_needed(self):
#         if self.last_send_time is None:
#             self.last_send_time = time()
#             return

#         elapsed = time() - self.last_send_time
#         if elapsed < self.min_interval:
#             sleep(self.min_interval - elapsed)

#         self.last_send_time = time()
