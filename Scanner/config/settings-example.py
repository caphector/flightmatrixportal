LOCATION_HOME = [
    37.57350,    # Latitude (deg)
    -122.00362,  # Longitude (deg)
]

JOURNEYBLANK = " - "

M_USER = ""
M_PASS = ""

FA_API_KEY = ''
RAPID_API_KEY = ''

LOG_FILE = 'daemon.log'

ADSB_URL = "http://127.0.0.1/data/aircraft.json"

PERSONAL_SPACE = 2  # Miles

MQTT_HOST = ""
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 45
FA_URL = 'https://aeroapi.flightaware.com/aeroapi/flights/'
ADB_URL = 'https://aerodatabox.p.rapidapi.com/flights/number/'
AIRPORTS = 'airports.csv'
ADSBDB_URL = 'https://api.adsbdb.com/v0/'
AIRCRAFT_TYPE = 'ICAOList.csv'
INFAMY_LIST = 'plane-alert-db.csv'
AIRLINES = 'airlines.csv'
MQTT_TOPIC = "flight/0"

CLOSE = '.close'
FLIGHT_DUMP = '.data'
API_DUMP = '.api'

SHORT = 20
LONG = 180
RETRIES = 3
PATH_THRESHOLD = 100 # Miles
