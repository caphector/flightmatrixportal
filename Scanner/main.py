# main.py

import random
import logging
import time
from datetime import datetime, timedelta
import json
from pathlib import Path
from typing import Optional, Dict, Any
import requests
from core.mqtt_handler import MQTTHandler
from core.api_limiter import APILimiter
from core.cache_manager import CacheManager
from api.flight_aware import FlightAwareAPI
from api.aero_data import AeroDataAPI
from api.adsb_db import ADSBdbAPI
from utils.data_loader import DataLoader
from utils.distance_calculator import calculate_distance
from models.flight import Flight

from config.settings import (
    MQTT_HOST, MQTT_PORT, M_USER, M_PASS, MQTT_TOPIC,
    FA_API_KEY, FA_URL, ADB_URL, ADSBDB_URL, RAPID_API_KEY,
    LOCATION_HOME, PERSONAL_SPACE, LOG_FILE, AIRLINES,
    AIRCRAFT_TYPE, INFAMY_LIST, SHORT, LONG, CLOSE,
    API_DUMP, JOURNEYBLANK, ADSB_URL
)


class FlightTracker:
    def __init__(self):
        # Initialize logging
        self._setup_logging()

        # Initialize components
        self.data_loader = DataLoader()
        self.cache_manager = CacheManager()

        # Initialize API handlers
        self.flight_aware = FlightAwareAPI(
            api_key=FA_API_KEY,
            url=FA_URL,
            limiter=APILimiter(2000, "FlightAware"),
            airlines_file=AIRLINES,
            aircraft_types_file=AIRCRAFT_TYPE,
            api_dump_file=API_DUMP,
            journey_blank=JOURNEYBLANK
        )
        self.aero_data = AeroDataAPI(
            api_key=RAPID_API_KEY,
            base_url=ADB_URL,
            limiter=APILimiter(400, "AeroDataBox"),
            airlines_file=AIRLINES,
            aircraft_types_file=AIRCRAFT_TYPE
        )
        self.adsb_db = ADSBdbAPI(
            base_url=ADSBDB_URL,
            limiter=APILimiter(5000, "ADSBDB"),
            airlines_file=AIRLINES
        )

        # Initialize MQTT
        self.mqtt = MQTTHandler(
            host=MQTT_HOST,
            port=MQTT_PORT,
            user=M_USER,
            password=M_PASS,
            topic=MQTT_TOPIC,
            messages_per_second=1.0
        )

        # Load reference data
        self.airlines = self.data_loader.load_airlines(AIRLINES)
        self.aircraft_types = self.data_loader.load_aircraft_types(AIRCRAFT_TYPE)
        self.infamy_list = self.data_loader.load_csv(INFAMY_LIST)

        # Set operating hours
        self.start_time = datetime.strptime('09:00', '%H:%M').time()
        self.end_time = datetime.strptime('23:59', '%H:%M').time()

    def _setup_logging(self):
        """Configure logging"""
        logging.basicConfig(
            filename=LOG_FILE,
            format='%(asctime)s %(levelname)-8s %(message)s',
            level=logging.INFO,
            datefmt='%Y-%m-%d %H:%M:%S'
        )
        console = logging.StreamHandler()
        console.setLevel('DEBUG')
        formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
        console.setFormatter(formatter)
        logging.getLogger("").addHandler(console)

    def get_flight_data(self, plane: Dict[str, Any]) -> Optional[Flight]:
        """Try all APIs to get flight data"""
        flight_data = None

        # Try each API in order
        apis = [
            (self.adsb_db, "ADSB-DB"),
            (self.flight_aware, "FlightAware"),
            (self.aero_data, "AeroDataBox")
        ]

        for api, name in apis:
            try:
                flight_data = api.get_flight_data(plane)
                if flight_data:
                    logging.info(f"Got flight data from {name}")
                    break
            except Exception as e:
                logging.error(f"Error getting data from {name}: {str(e)}")
                continue

        return flight_data

    def check_infamy(self, aircraft_data: Dict[str, Any]) -> Optional[Flight]:
        """Check for infamous aircraft"""
        for aircraft in aircraft_data.get('aircraft', []):
            hex_code = aircraft.get('hex', '').upper()
            matches = [row for row in self.infamy_list if hex_code in row]
            if matches:
                match = matches[0]
                return Flight(
                    origin='!!!',
                    destination='!!!',
                    flight_number=match[3],
                    plane=f"{match[6]} - {match[7]} - {match[8]} - {match[9]}",
                    vertical_speed=' ',
                    altitude=match[2],
                    hex=hex_code
                )
        return None

    def get_closest_plane(self, aircraft_data: Dict[str, Any]) -> Dict[str, Any]:
        """Find the closest aircraft"""
        closest_plane = None
        shortest_distance = float('inf')
        lowest_altitude = float('inf')

        for aircraft in aircraft_data.get('aircraft', []):
            distance = calculate_distance(aircraft, LOCATION_HOME)
            altitude = aircraft.get("alt_geom", aircraft.get("alt_baro", float('inf')))

            if isinstance(altitude, str):
                altitude = 0 if altitude == 'ground' else float('inf')

            if distance < shortest_distance:
                shortest_distance = distance
                closest_plane = aircraft

            if altitude < lowest_altitude and distance < PERSONAL_SPACE:
                lowest_altitude = altitude
                closest_plane = aircraft

        return closest_plane

    def process_aircraft_data(self, aircraft_data: Dict[str, Any]) -> None:
        """Process received aircraft data"""
        # Check for infamous aircraft first
        infamous = self.check_infamy(aircraft_data)
        if infamous:
            self.mqtt.publish(infamous.to_json(), retain=False)
            return

        # Get closest aircraft
        closest = self.get_closest_plane(aircraft_data)
        if not closest:
            self.mqtt.publish(Flight.create_empty().to_json(), retain=False)
            return

        # Calculate distance
        distance = calculate_distance(closest, LOCATION_HOME)

        if distance < PERSONAL_SPACE:
            # Get flight data from cache or APIs
            flight_id = closest.get('flight')
            hex_id = closest.get('hex')

            flight_data = self.cache_manager.get(flight_id, hex_id)
            if not flight_data:
                flight_data = self.get_flight_data(closest)
                if flight_data:
                    self.cache_manager.set(flight_id, hex_id, flight_data.to_dict())

            if flight_data:
                self.mqtt.publish(
                    json.dumps(flight_data if isinstance(flight_data, dict) else flight_data.to_dict()),
                    retain=False
                )
        else:
            self.mqtt.publish(Flight.create_empty().to_json(), retain=False)

    def run(self):
        """Main loop"""
        while True:
            try:
                current_time = datetime.now().time()

                # Check if within operating hours
                if self.start_time <= current_time <= self.end_time:
                    try:
                        # Get aircraft data from ADSB
                        response = requests.get(ADSB_URL, timeout=30)
                        if response.status_code == 200:
                            aircraft_data = response.json()
                            if aircraft_data:
                                self.process_aircraft_data(aircraft_data)

                    except requests.exceptions.RequestException as e:
                        logging.error(f"ADSB request error: {str(e)}")
                        time.sleep(SHORT)
                    except json.JSONDecodeError as e:
                        logging.error(f"JSON decode error: {str(e)}")
                        time.sleep(SHORT)
                    except Exception as e:
                        logging.error(f"Processing error: {str(e)}")
                        time.sleep(SHORT)
                else:
                    logging.info("Outside operating hours")
                    time.sleep(LONG)

                # Periodic maintenance
                if current_time.minute == 0 and current_time.second < 10:
                    self.cache_manager.cleanup_expired_files()

            except Exception as e:
                logging.error(f"Main loop error: {str(e)}")
                time.sleep(SHORT)

def main():
    tracker = FlightTracker()
    tracker.run()

if __name__ == "__main__":
    main()
