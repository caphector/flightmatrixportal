import logging
import requests
from typing import Optional, Dict, Any
from models.flight import Flight
from core.api_limiter import APILimiter
from utils.data_loader import DataLoader

class AeroDataAPI:
    def __init__(self, 
                 api_key: str, 
                 base_url: str, 
                 limiter: APILimiter,
                 airlines_file: str,
                 aircraft_types_file: str):
        self.base_url = base_url
        self.limiter = limiter
        self.headers = {
            "X-RapidAPI-Key": api_key,
            "X-RapidAPI-Host": "aerodatabox.p.rapidapi.com"
        }

        # Initialize DataLoader and load reference data
        self.data_loader = DataLoader()
        self.airlines = self.data_loader.load_airlines(airlines_file)
        self.aircraft_types = self.data_loader.load_aircraft_types(aircraft_types_file)

    def _get_airline_name(self, airline_code: str) -> str:
        """Get airline name from loaded airline data"""
        if not airline_code:
            return "General Aviation"

        airline_info = self.airlines.get(airline_code, {})
        return airline_info.get('name', "General Aviation")

    def _get_aircraft_info(self, aircraft_type: str) -> str:
        """Get aircraft information from loaded aircraft types data"""
        if not aircraft_type:
            return "Unknown"

        aircraft_info = self.aircraft_types.get(aircraft_type, {})
        if aircraft_info:
            manufacturer = aircraft_info['manufacturer']
            model = aircraft_info['model']
            if manufacturer == '(any manufacturer)':
                manufacturer = '!'
            return f"{manufacturer} {model}".strip()
        return "Unknown"

    def _process_response(self, response: Dict, plane: Dict) -> Optional[Flight]:
        """Process API response and create Flight object"""
        try:
            # Handle LADD blocked aircraft
            if isinstance(response, dict) and 'LADD Program' in response.get('message', ''):
                return self._create_ladd_flight(plane)

            # Get flight data
            flight_data = response[0]
            if flight_data.get('status') == 'Expected':
                logging.info(f"{flight_data.get('callsign')} not expected")
                return None

            # Extract flight information
            departure = flight_data.get('departure', {}).get('airport', {})
            arrival = flight_data.get('arrival', {}).get('airport', {})

            # Get airline information using loaded data
            airline_code = (flight_data.get('airline', {}).get('iata') or 
                          flight_data.get('airline', {}).get('icao'))
            airline = self._get_airline_name(airline_code)

            # Get origin and destination codes
            origin = self._get_airport_code(departure)
            destination = self._get_airport_code(arrival)

            # Get aircraft information using loaded data
            aircraft_type = flight_data.get('aircraft', {}).get('model')
            airframe = self._get_aircraft_info(aircraft_type)
            if not airframe or airframe == "Unknown":
                # Fallback to basic aircraft info from response
                airframe = (flight_data.get('aircraft', {}).get('model') or 
                          flight_data.get('airline', {}).get('name', 'Unknown'))

            # Create Flight object
            from_local = self._get_local_data(plane)
            return Flight(
                origin=self._clean_airport_code(origin),
                destination=self._clean_airport_code(destination),
                flight_number=plane.get('flight', ''),
                plane=airframe,
                vertical_speed=from_local.get('vertical_speed', 0),
                altitude=airline,
                hex=plane.get('hex', '')
            )

        except Exception as e:
            logging.error(f"Error processing AeroData response: {str(e)}")
            return None

    def _get_airport_code(self, airport_data: Dict) -> str:
        """Extract airport code from airport data"""
        codes = {key: val for key, val in airport_data.items() 
                if key.startswith('i') and val is not None}
        return (codes.get('iata') or 
                codes.get('icao') or 
                codes.get('localCode', ''))

    def _clean_airport_code(self, code: str) -> str:
        """Clean airport code (remove K prefix if present)"""
        if len(code) == 4 and code.startswith('K'):
            return code[1:]
        return code

    def _create_ladd_flight(self, plane: Dict) -> Flight:
        """Create Flight object for LADD blocked aircraft"""
        from_local = self._get_local_data(plane)
        return Flight(
            origin="PRI",
            destination="VATE",
            flight_number=plane.get('flight', ''),
            plane="Secret LADD",
            vertical_speed=from_local.get('vertical_speed', 0),
            altitude="General Aviation",
            hex=plane.get('hex', '')
        )

    def _get_local_data(self, airplane: Dict) -> Dict:
        """Extract local flight data"""
        return {
            'altitude': airplane.get("alt_geom", airplane.get("alt_baro", 1000000)),
            'vertical_speed': airplane.get("baro_rate", airplane.get("geom_rate", 0)),
            'hex': airplane.get('hex', '')
        }
