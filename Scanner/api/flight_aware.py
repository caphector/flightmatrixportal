import logging
import json
from datetime import datetime, timedelta
from typing import Optional, Dict, Any, List
import requests

from core.api_limiter import APILimiter
from utils.data_loader import DataLoader
from models.flight import Flight

class FlightAwareAPI:
    def __init__(self, 
                 api_key: str, 
                 url: str, 
                 limiter: APILimiter,
                 airlines_file: Optional[str] = None,
                 aircraft_types_file: Optional[str] = None,
                 api_dump_file: Optional[str] = None,
                 journey_blank: str = ''):
        self.api_key = api_key
        self.url = url
        self.limiter = limiter
        self.headers = {'x-apikey': api_key}
        self.api_dump_file = api_dump_file
        self.journey_blank = journey_blank

        # Initialize DataLoader and load reference data
        self.data_loader = DataLoader()
        self.airlines = (self.data_loader.load_airlines(airlines_file) 
                        if airlines_file else {})
        self.aircraft_types = (self.data_loader.load_aircraft_types(aircraft_types_file) 
                             if aircraft_types_file else {})

    def get_flight_data(self, plane: Dict[str, Any]) -> Optional[Flight]:
        """Get flight data from FlightAware API"""
        try:
            # Build query parameters
            query_params = self._build_query_params()
            query_flight = plane.get('flight')
            if not query_flight:
                return None

            # Execute query
            query = f"{self.url}{query_flight.strip()}{query_params}"
            logging.info(f"FlightAware Query: {query}")
            flights = self._make_api_request(query)
            if not flights:
                return None

            # Process response
            flight_data = self._process_flights(flights, plane)
            if not flight_data:
                return None

            return flight_data

        except Exception as e:
            logging.error(f"FlightAware API error: {str(e)}")
            return None

    def _build_query_params(self) -> str:
        """Build query parameters for API request"""
        now_utc = datetime.utcnow()
        range_end = now_utc + timedelta(hours=6)
        range_start = now_utc - timedelta(hours=47)
        range_end = range_end.isoformat('T', 'seconds')
        range_start = range_start.isoformat('T', 'seconds')
        return f'?start={range_start}Z&end={range_end}Z'

    def _make_api_request(self, url: str) -> Optional[Dict]:
        """Make API request with rate limiting"""
        if not self.limiter.can_make_request():
            logging.info("Rate limit reached")
            return None

        try:
            response = requests.get(url, headers=self.headers, timeout=30)
            self.limiter.consume_tokens()
            return response.json()
        except Exception as e:
            logging.error(f"API request failed: {str(e)}")
            return None

    def _process_flights(self, flights: Dict, plane: Dict) -> Optional[Flight]:
        """Process flight data from API response"""
        try:
            # Validate response
            num_flights = len(flights.get('flights', []))
            if num_flights == 0:
                logging.info('No flights found')
                return None

            # Get primary flight data
            flight = flights['flights'][0]

            # Log multiple flights if present
            if num_flights >= 2 and self.api_dump_file:
                from_local = self._get_local_data(plane)
                self.data_loader.save_text(
                    f"Query: {from_local}\nFA: {json.dumps(flights, indent=2)}\n",
                    self.api_dump_file
                )

            # Process flight details
            return self._create_flight_object(flight, plane)

        except (TypeError, KeyError, IndexError) as error:
            logging.error(f'Flight data error: {error}')
            return None

    def _get_airport_code(self, airport_data: Dict) -> str:
        """Extract airport code from airport data"""
        if not airport_data:
            return self.journey_blank

        codes = {
            key: val for key, val in airport_data.items() 
            if key.startswith('code') and val is not None
        }

        return (codes.get('code_iata') or 
                codes.get('code_lid') or 
                codes.get('code') or 
                codes.get('code_icao', self.journey_blank))

    def _clean_airport_code(self, code: str) -> str:
        """Clean airport code (remove K prefix if present)"""
        if len(code) == 4 and code.startswith('K'):
            return code[1:]
        return code

    def _get_airline_info(self, flight_data: Dict) -> str:
        """Get airline information"""
        airline_code = (flight_data.get('operator_icao') or 
                       flight_data.get('operator_iata') or 
                       flight_data.get('type'))

        if airline_code == 'General_Aviation':
            return 'General Aviation'

        if airline_code in self.airlines:
            return self.airlines[airline_code].get('name', 'General Aviation')

        return 'General Aviation'

    def _get_aircraft_info(self, aircraft_type: str) -> str:
        """Get aircraft information"""
        if not aircraft_type:
            return ' '

        aircraft_info = self.aircraft_types.get(aircraft_type, {})
        if aircraft_info:
            manufacturer = aircraft_info['manufacturer']
            if manufacturer == '(any manufacturer)':
                manufacturer = '!'
            return f"{manufacturer} {aircraft_info['model']}".strip()

        return ' '

    def _create_flight_object(self, flight: Dict, plane: Dict) -> Optional[Flight]:
        """Create Flight object from processed data"""
        try:
            # Get origin/destination
            origin = self._clean_airport_code(
                self._get_airport_code(flight.get('origin', {}))
            )
            destination = self._clean_airport_code(
                self._get_airport_code(flight.get('destination', {}))
            )

            # Get airline and aircraft info
            airline = self._get_airline_info(flight)
            airframe = self._get_aircraft_info(flight.get('aircraft_type', ''))

            # Check for special aircraft
            if plane_hex := plane.get('hex'):
                special_info = self._check_special_aircraft(plane_hex)
                if special_info:
                    airline = special_info[0][2]
                    airframe = (f"!!! - {airframe} - {special_info[0][6]} - "
                              f"{special_info[0][7]} - {special_info[0][8]} - "
                              f"{special_info[0][9]}'")
                    origin = '!' + origin

            # Get local data
            from_local = self._get_local_data(plane)

            return Flight(
                origin=origin,
                destination=destination,
                flight_number=plane.get('flight', ''),
                plane=airframe,
                vertical_speed=from_local.get('vertical_speed', 0),
                altitude=airline,
                hex=from_local.get('hex', '')
            )

        except Exception as e:
            logging.error(f"Error creating flight object: {str(e)}")
            return None

    def _check_special_aircraft(self, hex_code: str) -> Optional[List]:
        """Check if aircraft is special"""
        try:
            special_aircraft = self.data_loader.load_csv('plane-alert-db.csv')
            return [row for row in special_aircraft if hex_code.upper() in row]
        except Exception:
            return None

    def _get_local_data(self, airplane: Dict) -> Dict:
        """Extract local flight data"""
        return {
            'altitude': airplane.get("alt_geom", airplane.get("alt_baro", 1000000)),
            'vertical_speed': airplane.get("baro_rate", airplane.get("geom_rate", 0)),
            'hex': airplane.get('hex', '')
        }
