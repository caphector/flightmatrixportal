import logging
from typing import Optional, Dict, Any
from datetime import datetime
import requests
from models.flight import Flight
from utils.data_loader import DataLoader
from core.api_limiter import APILimiter

class ADSBdbAPI:
    def __init__(self, base_url: str, limiter: APILimiter, airlines_file: str):
        self.base_url = base_url
        self.limiter = limiter
        self.data_loader = DataLoader()
        self.airlines_data = self.data_loader.load_airlines(airlines_file)

    def get_flight_data(self, plane: Dict[str, Any]) -> Optional[Flight]:
        """
        Get flight data from ADSB DB API
        Args:
            plane: Dictionary containing flight information
        Returns:
            Flight object if successful, None otherwise
        """
        try:
            flight_number = plane.get('flight')
            hex_code = plane.get('hex')
            if not flight_number or not hex_code:
                logging.info("Missing flight number or hex code")
                return None

            # Build query URLs
            flight_query = f'{self.base_url}callsign/{flight_number.strip()}'
            aircraft_query = f'{self.base_url}aircraft/{hex_code}'

            # Get flight data
            flight_data = self._make_api_request(flight_query)
            if not self._validate_flight_response(flight_data):
                return None

            # Get aircraft data
            aircraft_data = self._make_api_request(aircraft_query)

            # Process the response
            return self._process_response(flight_data, aircraft_data, plane)

        except Exception as e:
            logging.error(f"ADSBDB API error: {str(e)}")
            return None

    def _make_api_request(self, url: str) -> Optional[Dict]:
        """Make API request with rate limiting"""
        if not self.limiter.can_make_request():
            logging.info("Rate limit reached")
            return None

        try:
            response = requests.get(url, timeout=30)
            self.limiter.consume_tokens()
            return response.json()
        except Exception as e:
            logging.error(f"API request failed: {str(e)}")
            return None

    def _validate_flight_response(self, response: Optional[Dict]) -> bool:
        """Validate API response"""
        if not response:
            return False

        response_text = response.get('response')
        if not response_text:
            return False

        # Check for error responses
        if isinstance(response_text, str):  # Make sure we're comparing strings
            error_messages = {
                'not found': "Flight not found",
                'rate limited for': "Rate limited",
                'unknown callsign': "Unknown callsign"
            }

            if response_text in error_messages:
                logging.info(f"ADSBDB result: {error_messages[response_text]}")
                return False

        return True


    def _process_response(self, flight_data: Dict, aircraft_data: Dict, plane: Dict) -> Optional[Flight]:
        """Process API response and create Flight object"""
        try:
            route = flight_data['response'].get('flightroute', {})

            # Get airline information
            try:
                airline = route['airline'].get('name', "General Aviation")
            except AttributeError:
                airline = "General Aviation"

            # Get aircraft information
            try:
                if aircraft_data.get('response') == 'unknown aircraft':
                    airframe = "Unknown"
                else:
                    aircraft_info = aircraft_data['response']['aircraft']
                    airframe = f"{aircraft_info.get('manufacturer', '')} {aircraft_info.get('type', '')}"
            except AttributeError:
                airframe = "Unknown"
                logging.info(f"ADSBDB Airframe issues: {aircraft_data}")

            # Get origin and destination
            origin = (route.get('origin', {}).get('iata_code') or 
                     route.get('origin', {}).get('icao_code', ''))
            destination = (route.get('destination', {}).get('iata_code') or 
                         route.get('destination', {}).get('icao_code', ''))

            # Create Flight object
            from_local = self._get_local_data(plane)
            return Flight(
                origin=origin,
                destination=destination,
                flight_number=plane.get('flight', ''),
                plane=airframe,
                vertical_speed=from_local.get('vertical_speed', 0),
                altitude=airline,
                hex=plane.get('hex', '')
            )

        except Exception as e:
            logging.error(f"Error processing ADSBDB response: {str(e)}")
            return None

    def _get_local_data(self, airplane: Dict) -> Dict:
        """Extract local flight data"""
        return {
            'altitude': airplane.get("alt_geom", airplane.get("alt_baro", 1000000)),
            'vertical_speed': airplane.get("baro_rate", airplane.get("geom_rate", 0)),
            'hex': airplane.get('hex', '')
        }
