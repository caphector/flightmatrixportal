import csv
import json
import logging
from typing import List, Dict, Any, Union, Optional
from pathlib import Path
from datetime import datetime
import yaml
from json.decoder import JSONDecodeError
from contextlib import contextmanager

class DataLoadError(Exception):
    """Custom exception for data loading errors"""
    pass

class DataLoader:
    def __init__(self, cache_enabled: bool = True):
        self.cache_enabled = cache_enabled
        self._cache: Dict[str, tuple[Any, datetime]] = {}
        self._cache_duration = 300  # 5 minutes in seconds

    @contextmanager
    def _file_handler(self, file_path: Union[str, Path], mode: str = 'r', encoding: str = 'utf8'):
        """Context manager for file operations with error handling"""
        file_path = Path(file_path)
        try:
            with open(file_path, mode=mode, encoding=encoding) as file:
                yield file
        except FileNotFoundError:
            logging.error(f"File not found: {file_path}")
            raise DataLoadError(f"File not found: {file_path}")
        except PermissionError:
            logging.error(f"Permission denied: {file_path}")
            raise DataLoadError(f"Permission denied: {file_path}")
        except Exception as e:
            logging.error(f"Error handling file {file_path}: {str(e)}")
            raise DataLoadError(f"Error handling file {file_path}: {str(e)}")

    def _is_cache_valid(self, cache_time: datetime) -> bool:
        """Check if cached data is still valid"""
        return (datetime.now() - cache_time).total_seconds() < self._cache_duration

    def load_csv(self, 
                file_path: Union[str, Path], 
                delimiter: str = ',', 
                headers: bool = False) -> List[List[str]]:
        """
        Load data from CSV file
        Args:
            file_path: Path to CSV file
            delimiter: CSV delimiter character
            headers: Whether to return headers as first row
        Returns:
            List of rows from CSV file
        """
        cache_key = f"csv_{file_path}_{delimiter}_{headers}"

        # Check cache first
        if self.cache_enabled and cache_key in self._cache:
            data, cache_time = self._cache[cache_key]
            if self._is_cache_valid(cache_time):
                return data

        try:
            with self._file_handler(file_path) as file:
                csv_reader = csv.reader(file, delimiter=delimiter)
                data = list(csv_reader)

                if not headers and data:  # Remove header row if not requested
                    data = data[1:]

                # Update cache
                if self.cache_enabled:
                    self._cache[cache_key] = (data, datetime.now())

                return data

        except csv.Error as e:
            logging.error(f"CSV error in file {file_path}: {str(e)}")
            raise DataLoadError(f"CSV error in file {file_path}: {str(e)}")

    def load_json(self, file_path: Union[str, Path]) -> Dict[str, Any]:
        """
        Load data from JSON file
        Args:
            file_path: Path to JSON file
        Returns:
            Dictionary containing JSON data
        """
        cache_key = f"json_{file_path}"

        # Check cache first
        if self.cache_enabled and cache_key in self._cache:
            data, cache_time = self._cache[cache_key]
            if self._is_cache_valid(cache_time):
                return data

        try:
            with self._file_handler(file_path) as file:
                data = json.load(file)

                # Update cache
                if self.cache_enabled:
                    self._cache[cache_key] = (data, datetime.now())

                return data

        except JSONDecodeError as e:
            logging.error(f"JSON decode error in file {file_path}: {str(e)}")
            raise DataLoadError(f"JSON decode error in file {file_path}: {str(e)}")

    def save_json(self, 
                 data: Dict[str, Any], 
                 file_path: Union[str, Path], 
                 indent: int = 2) -> None:
        """
        Save data to JSON file
        Args:
            data: Dictionary to save
            file_path: Path to save JSON file
            indent: Number of spaces for indentation
        """
        try:
            with self._file_handler(file_path, mode='w') as file:
                json.dump(data, file, indent=indent)

            # Invalidate cache
            cache_key = f"json_{file_path}"
            self._cache.pop(cache_key, None)

        except TypeError as e:
            logging.error(f"JSON serialization error: {str(e)}")
            raise DataLoadError(f"JSON serialization error: {str(e)}")

    def load_yaml(self, file_path: Union[str, Path]) -> Dict[str, Any]:
        """
        Load data from YAML file
        Args:
            file_path: Path to YAML file
        Returns:
            Dictionary containing YAML data
        """
        cache_key = f"yaml_{file_path}"

        # Check cache first
        if self.cache_enabled and cache_key in self._cache:
            data, cache_time = self._cache[cache_key]
            if self._is_cache_valid(cache_time):
                return data

        try:
            with self._file_handler(file_path) as file:
                data = yaml.safe_load(file)

                # Update cache
                if self.cache_enabled:
                    self._cache[cache_key] = (data, datetime.now())

                return data

        except yaml.YAMLError as e:
            logging.error(f"YAML error in file {file_path}: {str(e)}")
            raise DataLoadError(f"YAML error in file {file_path}: {str(e)}")

    def load_text(self, file_path: Union[str, Path]) -> str:
        """
        Load data from text file
        Args:
            file_path: Path to text file
        Returns:
            String containing file contents
        """
        cache_key = f"text_{file_path}"

        # Check cache first
        if self.cache_enabled and cache_key in self._cache:
            data, cache_time = self._cache[cache_key]
            if self._is_cache_valid(cache_time):
                return data

        try:
            with self._file_handler(file_path) as file:
                data = file.read()

                # Update cache
                if self.cache_enabled:
                    self._cache[cache_key] = (data, datetime.now())

                return data

        except UnicodeDecodeError as e:
            logging.error(f"Text decode error in file {file_path}: {str(e)}")
            raise DataLoadError(f"Text decode error in file {file_path}: {str(e)}")

    def save_text(self, text: str, file_path: Union[str, Path]) -> None:
        """
        Save text to file
        Args:
            text: String to save
            file_path: Path to save text file
        """
        try:
            with self._file_handler(file_path, mode='w') as file:
                file.write(text)

            # Invalidate cache
            cache_key = f"text_{file_path}"
            self._cache.pop(cache_key, None)

        except UnicodeEncodeError as e:
            logging.error(f"Text encode error: {str(e)}")
            raise DataLoadError(f"Text encode error: {str(e)}")

    def load_aircraft_types(self, file_path: Union[str, Path]) -> Dict[str, Dict[str, str]]:
        """
        Load aircraft type data from CSV
        Args:
            file_path: Path to aircraft types CSV file
        Returns:
            Dictionary mapping aircraft codes to their details
        """
        try:
            rows = self.load_csv(file_path)
            aircraft_types = {}

            for row in rows:
                if len(row) >= 3:  # Ensure row has minimum required fields
                    aircraft_code = row[0].strip()
                    aircraft_types[aircraft_code] = {
                        'manufacturer': row[1].strip().replace('"', '').title(),
                        'model': row[2].strip().replace('"', '')
                    }

            return aircraft_types

        except Exception as e:
            logging.error(f"Error loading aircraft types: {str(e)}")
            raise DataLoadError(f"Error loading aircraft types: {str(e)}")

    def load_airlines(self, file_path: Union[str, Path]) -> Dict[str, Dict[str, str]]:
        """
        Load airline data from CSV
        Args:
            file_path: Path to airlines CSV file
        Returns:
            Dictionary mapping airline codes to their details
        """
        try:
            rows = self.load_csv(file_path)
            airlines = {}

            for row in rows:
                if len(row) >= 3:  # Ensure row has minimum required fields
                    airline_code = row[0].strip()
                    airlines[airline_code] = {
                        'name': row[2].strip(),
                        'country': row[1].strip() if len(row) > 3 else ''
                    }

            return airlines

        except Exception as e:
            logging.error(f"Error loading airlines: {str(e)}")
            raise DataLoadError(f"Error loading airlines: {str(e)}")

    def clear_cache(self) -> None:
        """Clear the data cache"""
        self._cache.clear()
