import geopy.distance as geodist

def calculate_distance(flight_coords, home_coords):
    try:
        if isinstance(flight_coords, dict):
            coords = (flight_coords['lat'], flight_coords['lon'])
        else:
            coords = flight_coords
        return geodist.geodesic(coords, home_coords).miles
    except (KeyError, TypeError):
        return float('inf')
