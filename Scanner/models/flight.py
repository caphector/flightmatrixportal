from dataclasses import dataclass, asdict
import json
from typing import Optional

@dataclass
class Flight:
    origin: str
    destination: str
    flight_number: str
    plane: str
    vertical_speed: float
    altitude: str
    hex: str

    def to_dict(self) -> dict:
        """Convert Flight object to dictionary"""
        return asdict(self)

    def to_json(self) -> str:
        """Convert Flight object to JSON string"""
        return json.dumps(self.to_dict())

    @classmethod
    def create_empty(cls) -> 'Flight':
        """Create an empty Flight object"""
        return cls(
            origin='',
            destination='',
            flight_number='',
            plane='',
            vertical_speed='',
            altitude='',
            hex=''
        )
