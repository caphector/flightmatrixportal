stages:
  - lint
  - test
  - coverage
  - quality
  - deploy

generate code score:
  stage: lint
  image: registry.gitlab.com/pipeline-components/pylint:latest
  variables:
    disable: C0301,E0401,E0611
    PIP_ROOT_USER_ACTION: ignore
  before_script:
    - mkdir -p public/badges public/lint
    - echo undefined > public/badges/$CI_JOB_NAME.score
    - echo "$pylintrc" > .pylintrc
  script:
    - pylint --exit-zero --disable $disable --load-plugins=pylint_gitlab $(find -type f -name "*.py") | tee pylint_format.txt
    - pylint --exit-zero --disable $disable --load-plugins=pylint_gitlab --output-format=gitlab-codeclimate $(find -type f -name "*.py") > pylint.txt
    - pylint --exit-zero --disable $disable --output-format=pylint_gitlab.GitlabCodeClimateReporter $(find -type f -name "*.py") > codeclimate.json
    - pylint --exit-zero --disable $disable --output-format=pylint_gitlab.GitlabPagesHtmlReporter $(find -type f -name "*.py") > public/lint/index.html
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' pylint_format.txt > public/badges/$CI_JOB_NAME.score
   # - pip install pytest pytest-cov
   # - pytest -v --cov --cov-report term --cov-report xml:coverage.xml
  after_script:
    - anybadge --overwrite --label $CI_JOB_NAME --value=$(cat public/badges/$CI_JOB_NAME.score) --file=public/badges/$CI_JOB_NAME.svg 4=red 6=orange 8=yellow 10=green
    - |
      echo "Your score is: $(cat public/badges/$CI_JOB_NAME.score)"
  artifacts:
    reports:
      #codequality: codeclimate.json
      #codequality: coverage.xml
    when: always
    paths:
      - public
      - pylint.txt
      - pylint_format.txt
      - covarege.xml


variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.pip-cache"
  PYTEST_ADDOPTS: "--color=yes"

cache:
  key: "${CI_JOB_NAME}"
  paths:
    - .pip-cache/
    - venv/

before_script:
  - python -V
  - python -m venv venv
  - source venv/bin/activate
  - pip install --upgrade pip
  - pip install -r requirements.txt
  - pip install -r requirements-dev.txt

lint:
  stage: lint
  script:
    # Lint all components
    - flake8 scanner MatrixPortal SwiftBar
    - black --check scanner MatrixPortal SwiftBar
    - isort --check-only scanner MatrixPortal SwiftBar
  allow_failure: true

test:
  stage: test
  script:
    # Only run tests for scanner component
    - cd scanner
    - pytest test_* -v --junitxml=../report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml
    expire_in: 1 week

coverage:
  stage: coverage
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  script:
    # Generate coverage only for scanner
    - cd scanner
    - pytest test_* --cov=. --cov-report=xml:../coverage.xml --cov-report=html:../htmlcov --cov-report=term
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - htmlcov/
    expire_in: 1 week

quality:
  stage: quality
  script:
    # Run quality checks on all components
    - pylint scanner MatrixPortal SwiftBar
    - mypy scanner MatrixPortal SwiftBar
  allow_failure: true


pages:
  stage: deploy
  image: alpine:latest
  needs:
    - job: generate code score
      artifacts: true
  script:
    - echo
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH