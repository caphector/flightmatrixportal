Code for my MatrixPortal "What is that flight?" display.

The MatrixPortal folder go on a MatrixPortal. The device will connect to WiFi and subscribe to the `flight/0` topic for data to display.

The files in Scanner are used to configure the flight scanning daemon, which needs a MQTT server to talk to. It will post to a `flight` topic

The scanner uses FlightAware (flightaware.com) and AeroDataBox (https://aerodatabox.com) APIs for flight data; each is a limited-use API and the daemon has rate limiting built in to mitigate excessive usage.

![LED Display](https://caphector.com/gallery/display.jpg)

![Menu bar display](https://caphector.com/gallery/menu.png)
