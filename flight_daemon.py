# Data files:
# https://github.com/rikgale/plane-alert-db/blob/main/plane-alert-db.csv
# https://github.com/rikgale/ICAOList/blob/main/ICAOList.csv
# https://en.wikipedia.org/wiki/List_of_airline_codes to `airlines.csv` via
# https://wikitable2csv.ggor.de
import random
from time import sleep
import json
from json import JSONDecodeError
from decimal import *
import logging
import time
import datetime
import csv
import geopy.distance as geodist
import requests
import json_stream.requests
from paho.mqtt import client as mqtt_client
from pyrate_limiter import SQLiteBucket, Limiter, Duration, RequestRate, BucketFullException
from requests_ratelimiter import LimiterAdapter
from requests import Session

from config import (
    ADB_URL,
    ADSB_URL,
    AIRCRAFT_TYPE,
    API_DUMP,
    FA_API_KEY,
    CLOSE,
    FA_URL,
    # FLIGHT_DUMP,
    JOURNEYBLANK,
    LOCATION_HOME,
    LOG_FILE,
    LONG,
    M_PASS,
    M_USER,
    MQTT_HOST,
    # MQTT_KEEPALIVE_INTERVAL,
    MQTT_PORT,
    MQTT_TOPIC,
    PERSONAL_SPACE,
    RAPID_API_KEY,
    SHORT,
    INFAMY_LIST,
    RETRIES,
    AIRLINES
)

LOCATION_DEFAULT = LOCATION_HOME
FA_HEADER = {'x-apikey': FA_API_KEY}
ADB_HEADER = {
    "X-RapidAPI-Key": RAPID_API_KEY,
    "X-RapidAPI-Host": "aerodatabox.p.rapidapi.com",
}

start = datetime.time(10, 0)
end = datetime.time(23, 59)
MQTT_CLIENT_ID = f'python-mqtt-{random.randint(0, 1000)}'

FA_Limits = (
    RequestRate(1, Duration.MINUTE),
    RequestRate(30, Duration.HOUR),
    RequestRate(66, Duration.DAY),
    RequestRate(2000, Duration.MONTH),
)

ADB_Limits = (
    RequestRate(3, Duration.HOUR),
    RequestRate(13, Duration.DAY),
    RequestRate(400, Duration.MONTH),
)

FlightAware = Limiter(
    *FA_Limits,
    bucket_class=SQLiteBucket,
    bucket_kwargs={'path': 'pyrate_limiter.sqlite'}
)

AeroDataBox = Limiter(
    *ADB_Limits,
    bucket_class=SQLiteBucket,
    bucket_kwargs={'path': 'pyrate_limiter.sqlite'}
)

FlightAware_Adapter = LimiterAdapter(limiter=FlightAware, max_delay=300)
AeroDataBox_Adapter = LimiterAdapter(limiter=AeroDataBox, max_delay=300)

Query = Session()

Query.mount(FA_URL, FlightAware_Adapter)
Query.mount(ADB_URL, AeroDataBox_Adapter)

Limiter.try_acquire(FlightAware)


def load_last():
    """Load cached flight data"""
    try:
        file = open(CLOSE, 'r', encoding="utf8")
        PREVIOUS = file.read()
    except FileNotFoundError:
        PREVIOUS = ''
    file.close()
    return PREVIOUS


def dump_data(data, file):
    """Dump data to file"""
    file = open(file, 'w+', encoding="utf8")
    file.write(data)
    file.close()


def load_flight_from_cache(file):
    """Load cache to json object"""
    with open(file) as flight_cache:
        cached_data = json.loads(flight_cache.read())
        file.close()
        return cached_data


def load_data(csv_file):
    """Read CSV file to list of rows"""
    lines = []
    data = open(csv_file, 'r', encoding="utf8")
    file_reader = csv.reader(data, delimiter=",")
    for row in file_reader:
        lines.append(row)
    return lines


logging.basicConfig(
    filename=LOG_FILE,
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

console = logging.StreamHandler()
console.setLevel('DEBUG')
formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
console.setFormatter(formatter)
logging.getLogger("").addHandler(console)
ListofAirlines = load_data(AIRLINES)


def connect_mqtt():
    """Connect to MQTT"""
    def on_connect(client, userdata, flags, rc):
        client.subscribe(MQTT_TOPIC)
        if rc == 0:
            # logging.info("Connected to MQTT")
            pass
        else:
            logging.info("Failed to connect, return code %d\n", rc)
    client = mqtt_client.Client(MQTT_CLIENT_ID)
    client.username_pw_set(M_USER, M_PASS)
    client.on_connect = on_connect
    client.connect(MQTT_HOST, MQTT_PORT)
    return client


def publish(MQTT_MESSAGE, MQTT_TOPIC, MQTT_CLIENT, RETAIN):
    """Publish MQTT Message"""
    MQTT_CLIENT.reconnect()
    result = MQTT_CLIENT.publish(MQTT_TOPIC, MQTT_MESSAGE, qos=0, retain=RETAIN)
    status = result[0]
    if status == 0:
        if not MQTT_MESSAGE.startswith('{"origin": "", "destination": ""'):
            logging.info(f"Sent `{MQTT_MESSAGE}` to topic `{MQTT_TOPIC}`")
    else:
        logging.info(f"Failed to send message to topic {MQTT_TOPIC}")


def distance_from_flight_to_home(flight, home=LOCATION_HOME):
    """Calculate geodisic distance to flight (ignores altitude)"""
    try:
        flight_coords = (flight['lat'], flight['lon'])
    except (KeyError, TypeError):
        try:
            flight_coords = (
                flight['lastPosition']['lat'],
                flight['lastPosition']['lon']
            )
        except (KeyError, TypeError):
            flight_coords = (0, 0)
    dist = geodist.geodesic(flight_coords, home).miles
    return dist


def get_fa_data(plane):
    """Take an airplane dict (airplane from an airplane.json)
    Looks up the flight in the FlightAware API
    Returns data for MQTT display
    https://flightaware.com/aeroapi/portal/documentation#get-/flights/-ident-
    Limit of 2k queries / month when user is ADS-B feeder
    """
    from_local = data_from_local(plane)
    now_utc = datetime.datetime.utcnow()
    range_end = now_utc + datetime.timedelta(hours=6)
    range_start = now_utc - datetime.timedelta(hours=47)
    range_end, range_start = range_end.isoformat('T', 'seconds'), range_start.isoformat('T', 'seconds')
    args = f'?start={range_start}&end={range_end}'
    query_flight = plane.get('flight')
    try:
        query = FA_URL + query_flight.strip() + args
    except AttributeError:
        logging.info(f"FA Attribute error: {plane}")
        return
    logging.info(f"FlightAware Query: {query}")
    flights = retry_query(query, FA_HEADER, None)
    logging.info("Query finished")
    logging.info(f"FA: {flights}")
    try:
        num_flights = len(flights['flights'])
    except (TypeError, KeyError) as error:
        logging.info(f'Flight data error {error}, {flights}')
        return
    try:
        if num_flights == 0:
            return False
    except (TypeError, IndexError, KeyError):
        logging.info('No data from FlightAware')
        pass
    try:
        if num_flights >= 1:
            flight = flights['flights'][0]
        if num_flights >= 2:
            dump_data(f"Query: {from_local}\nFA: {json.dumps(flights, indent=2)}\n", API_DUMP)
    except (TypeError, IndexError, KeyError):
        if flight is not None:
            logging.info(f"FA Type/Index Error: {json.dumps(flights, indent=2)}")
        return False
    try:
        if flight['diverted'] == 'true':
            diverted = flight['diverted']
            logging.info('Diverted {}'.format(diverted))
    except KeyError:
        pass
    try:
        flight_origin, flight_dest = JOURNEYBLANK, JOURNEYBLANK
        if flight['origin']:
            flight_origin = {key: val for key, val in flight['origin'].items() if key.startswith('code') and val is not None}
            origin = flight_origin.get('code_iata') or flight_origin.get('code_lid') or flight_origin.get('code') or flight_origin.get('code_icao', JOURNEYBLANK)
        if flight['destination']:
            flight_dest = {key: val for key, val in flight['destination'].items() if key.startswith('code') and val is not None}
            dest = flight_dest.get('code_iata') or flight_dest.get('code_lid') or flight_dest.get('code') or flight_dest('code_icao', JOURNEYBLANK)
        airline_code = flight.get('operator_icao') or flight.get('operator_iata') or flight.get('type')
        if airline_code != 'General_Aviation':
            airline = []
            airline = [row for row in ListofAirlines if airline_code in row]
            try:
                airline = airline[0][2]
            except IndexError:
                logging.info(f"Airline code failure: {flight}")
                airline = 'General Aviation'
        else:
            airline = 'General Aviation'
        if len(origin) == 4:
            if origin.startswith('K'):
                origin = origin[1:]
        if len(dest) == 4:
            if dest.startswith('K'):
                dest = dest[1:]
    except AttributeError as error:
        logging.info(f"FA error: {flight}, {error}")
    try:
        aircraft_type = flight['aircraft_type']
        data = open(AIRCRAFT_TYPE, 'r')
        airframes = data.readlines()
        for line in airframes:
            if line.find(aircraft_type) != -1:
                airframe = line.split(',')[-2:]
                this_mfg = airframe[0]
                this_airframe = airframe[1]
                this_mfg = this_mfg.replace('"', '').title()
                if this_mfg == '(any manufacturer)':
                    this_mfg = ' ! '
                this_airframe = this_airframe.replace('"', '')
                plane = this_mfg + ' ' + this_airframe.strip()
    except (TypeError, KeyError) as error:
        logging.info(f"FA error: {aircraft_type}, {error}")
        plane = ' '
    from_local = json.loads(json.dumps(from_local))
    this_flight = {
            "origin": origin,
            "destination": dest,
            "flight": query_flight,
            "plane": plane,
            "vertical_speed": from_local[0]['vertical_speed'],
            "altitude": airline,
            "hex": from_local[0]['hex'],
        }
    logging.info(f"FA result: {this_flight}")
    return this_flight


def data_from_local(airplane):
    this_flight = []
    altitude = airplane.get("alt_geom", airplane.get("alt_baro", 1000000))
    vertical_speed = airplane.get("baro_rate", airplane.get("geom_rate", 0))
    if 'hex' in airplane:
        hex = airplane.get('hex')
    this_flight.append(
        {
            'altitude': altitude,
            "vertical_speed": vertical_speed,
            'hex': hex,
        }
    )
    return this_flight


def retry_query(URL, HEAD, ARGS):
    """Handles the URL load and exceptions"""
    logging.info(f"Trying {URL}")
    ARG = {} if ARGS is None else ARGS
    for _ in range(RETRIES):
        try:
            logging.info(f"Trying query at {URL}")
            MyQuery = Query.get(URL, headers=HEAD, params=ARG, timeout=30)
            # logging.info(f"{MyQuery.text}")
            # if _ > 1:
            # 	logging.info(f"Retry_query: {URL}, {_}")
            # logging.info(f"Query: {URL}")
            break
        except BucketFullException as BucketError:
            if handle_rate_limit(BucketError) is False:
                logging.info("retry_query bucket limit exception")
                return
        except TimeoutError:
            logging.info(f'{URL} timeout')
            sleep(SHORT)
    try:
        result = json.loads(MyQuery.text)
    except JSONDecodeError:
        logging.info(f'JSON error - {URL}')
        # request_flight_info(plane, ' ')
        # logging.info(f"query_result was {result}")
    return result


def get_aerodata_data(plane):
    """Take an airplane dict (airplane from an airplane.json)
    Looks up the flight in the Aerodata API
    Returns data for MQTT display
    https://doc.aerodatabox.com/#tag/Flight-API/operation/GetFlight
    Limit of 400 queries / month at free tier
    """
    flight_num = 0
    args = {"withAircraftImage": "false", "withLocation": "false"}
    from_local = data_from_local(plane)
    AeroDataLink = ADB_URL + plane.get('flight').strip()
    logging.info(f"AeroDataBox Query: {AeroDataLink}")
    try:
        flight = retry_query(AeroDataLink, ADB_HEADER, args)
    except AttributeError:
        logging.info(f"ADB failure: {plane}")
        return
    # logging.info(json.dumps(flight, indent=2))
    try:
        if flight[flight_num].get('status') == 'Expected':
            logging.info(f"{flight[flight_num].get('callsign')} not expected")
            flight_num += flight_num
            # Do something with status = EnRoute ?
    except (TypeError, KeyError):
        logging.info(f"ADB Status TypeErr - {flight}\nPlane: {plane}")
        return
    try:
        flight_origin = flight[flight_num].get('departure')
        flight_dest = flight[flight_num].get('arrival')
        airline = flight[flight_num]['airline'].get('name')
        if airline == 'Unknown/Private owner':
            airline = "General Aviation"
    except (TypeError, KeyError):
        logging.info(f"ADB TypeErr - {flight}\nPlane: {plane}")
        return
    this_origin = {key: val for key, val in flight_origin['airport'].items() if key.startswith('i') and val is not None}
    this_dest = {key: val for key, val in flight_dest['airport'].items() if key.startswith('i') and val is not None}
    origin = this_origin.get('iata') or this_origin.get('icao') or this_origin.get('localCode', JOURNEYBLANK)
    dest = this_dest.get('iata') or this_dest.get('icao') or this_dest.get('localCode', JOURNEYBLANK)
    if len(origin) == 4:
        if origin.startswith('K'):
            origin = origin[1:]
    if len(dest) == 4:
        if dest.startswith('K'):
            dest = dest[1:]
    aircraft = {key: val for key, val in flight[flight_num].items() if key.startswith('air') and val is not None}
    try:
        airframe = aircraft['aircraft'].get('model') or aircraft['airline'].get('name')
    except KeyError:
        logging.info(f"ADB no airframe data: {flight}")
        airframe = ' '
    this_flight = {
            "origin": origin,
            "destination": dest,
            "flight": plane.get('flight'),
            "plane": airframe,
            "vertical_speed": from_local[0]['vertical_speed'],
            "altitude": airline,
            "hex": from_local[0]['hex'],
        }
    logging.info(f"FA result: {this_flight}")
    return this_flight


def request_flight_info(plane, hex):
    """Checks the various APIs for flight data"""
# 	logging.info(f"Checking {plane}")
    try:
        FlightAware.try_acquire('FlightAware')
        flight = get_fa_data(plane)
    except BucketFullException as err:
        if handle_rate_limit(err):
            logging.info(f'FA sleep - {err.meta_info.get("remaining_time")}')
            sleep(err.meta_info.get('remaining_time'))
            
        else:
            flight = None
            pass
    if flight:
        return flight
    try:
        AeroDataBox.try_acquire('AeroDataBox')
        flight = get_aerodata_data(plane)
    except BucketFullException as err:
        if handle_rate_limit(err):
            logging.info(f"ADB sleep - {err.meta_info.get('remaining_time')}")
            sleep(err.meta_info.get('remaining_time'))
        else:
            flight = None
            pass
    return flight


def handle_rate_limit(error):
    """Handler for rate limit exceptions"""
    TimeRemaining = error.meta_info.get('remaining_time')
    if TimeRemaining > 120:
        logging.info(f"{error.meta_info.get('identity')} skipped: {error.meta_info.get('rate')} {str(datetime.timedelta(seconds=TimeRemaining))}")
        return False
    return TimeRemaining


def check_infamy(latest_json):
    """Watching for famous or infamous planes
    Uses a list of aircraft, search for tail numbers (hex)
    and outputs a couple fields
    """
    infamy_list = load_data(INFAMY_LIST)
    # gossip = []
    for af in latest_json['aircraft']:
        inf = [row for row in infamy_list if af['hex'].upper() in row]
        if inf:
            s = f'{inf[0][3]} - {inf[0][6]} - {inf[0][7]} - {inf[0][8]} - {inf[0][9]}'
            try:
                vs = ' '
                # vs =  af.get('mach') or af.get('vertical_speed') or  ' '
                alt = af.get('altitude') or ' '
                hex = af.get('hex') or ' '
            except KeyError:
                print(f'check_imfamy key error: {af}')
            # vs, alt, hex = ' ', ' ', af[0].get('hex')
            gossip = {
                    "origin": '!!!',
                    "destination": '!!!',
                    "flight": inf[0][3],
                    "plane": s,
                    "vertical_speed": vs,
                    "altitude": inf[0][2],
                    "hex": hex,
                }
            logging.info(f'Famous:\n{gossip}\nData: {inf}\nRaw: {af}')
            return gossip


def get_closest_plane(latest_json):
    """Checks ADSB data to see which plane is nearest"""
    data = latest_json
    shortest_distance = ''
    lowest_altitude = ''
    for a_flight in data['aircraft']:
        my_dist = distance_from_flight_to_home(a_flight, home=LOCATION_HOME)
        my_alt = a_flight.get("alt_geom", a_flight.get("alt_baro", 999999))
        if my_alt == 'ground':
            my_alt = 0
        if not shortest_distance:
            shortest_distance = my_dist
            closest_plane = a_flight
        if not lowest_altitude:
            lowest_altitude = my_alt
            lowest_plane = a_flight
        try:
            if my_alt < lowest_altitude:
                lowest_altitude = my_alt
                lowest_plane = a_flight
        except TypeError:
            logging.info(f"this_alt: {my_alt}, lowest: {lowest_altitude}")
        try:
            if my_dist < shortest_distance:
                shortest_distance = my_dist
                closest_plane = a_flight
        except TypeError:
            logging.info(f"this: {my_dist}, shortest: {shortest_distance}")
        if lowest_plane != closest_plane:
            low = distance_from_flight_to_home(lowest_plane, home=LOCATION_HOME)
            raw = distance_from_flight_to_home(closest_plane, home=LOCATION_HOME)
            difference = abs(low - raw)
            if low < 3:
                if difference < 1:
                    try:
                        logging.info(f"Low alt: {low} Closest: {raw}, Diff: {difference}, Alt: {lowest_plane['altitude']}, C alt: {closest_plane['altitude']}")
                    except (KeyError, TypeError):
                        # logging.info(f"Alt:{lowest_plane}N:{closest_plane}")
                        pass
    return closest_plane


MQTT_CLIENT = connect_mqtt()

# Main function


def do_this_thing(latest_response):
    """
    Main function; checks for nearby planes (and infamous ones),
    queries as needed, then publishes to MQTT
    """
    prev_flight = {
        "origin": '',
        "destination": '',
        "flight": '',
        "plane": '',
        "vertical_speed": '',
        "altitude": '',
        "hex": ''
    }
    a_plane = get_closest_plane(latest_response)
    infamous = check_infamy(latest_response)
    prev_flight = load_last()
    distance = distance_from_flight_to_home(a_plane, home=LOCATION_HOME)
    aflight = a_plane.get('flight'), a_plane.get('hex')
    if distance < PERSONAL_SPACE:
        logging.info("Nearby plane spotted - Query for MQTT")
        if aflight is None:
            logging.info(f"No data {a_plane.get('hex')}\nRaw: {a_plane}")
            return
        logging.info(f"R: {a_plane.get('hex')}, f {aflight}, D {distance}")
        try:
            if aflight != prev_flight:
                logging.info("Fresh flight")
                try:
                    flight_info = request_flight_info(a_plane, a_plane.get('hex'))
                    if flight_info:
                        prev_flight = flight_info
                        # publish(json.dumps(flight_info[0]), MQTT_TOPIC, MQTT_CLIENT, True)
                        publish(json.dumps(flight_info), MQTT_TOPIC, MQTT_CLIENT, True)
                        logging.info("Published to MQTT")
                        wait_with_mqtt(LONG)
                except TypeError as error:
                    if a_plane is not None:
                        logging.info(f"No flight info: {error}")
            if aflight == prev_flight:
                logging.info("Flight still overhead - using cached data")
                publish(json.dumps(prev_flight), MQTT_TOPIC, MQTT_CLIENT, True)
                logging.info(f"Sleeping - Cached data {aflight}")
                wait_with_mqtt(LONG)
        except UnboundLocalError:
            prev_flight = a_plane
        prev_flight = a_plane
        dump_data(str(aflight), CLOSE)
        logging.info(f"H: {a_plane.get('hex')} F: {aflight} D: {distance}")
        wait_with_mqtt(SHORT)
    if infamous:
        logging.info("Famous plane spotted - publishing")
        publish(json.dumps(infamous), MQTT_TOPIC, MQTT_CLIENT, True)
        logging.info(f"Sleeping - Cached data {infamous}")
        wait_with_mqtt(LONG)
    else:
        data = {
            "origin": '',
            "destination": '',
            "flight": '',
            "plane": '',
            "vertical_speed": '',
            "altitude": '',
            "hex": '',
        }
        publish(json.dumps(data), MQTT_TOPIC, MQTT_CLIENT, True)
        wait_with_mqtt(SHORT)


def wait_with_mqtt(wait_time):
    if not wait_time:
        wait_time == SHORT
    end_time = time.time() + wait_time
    while time.time() < end_time:
        time.sleep(1)
        check_mqtt()


def check_mqtt():
    ResponseCode = MQTT_CLIENT.loop()
    if ResponseCode != mqtt_client.MQTT_ERR_SUCCESS:
        reconnect = 1
        try:
            time.sleep(reconnect)
            try:
                MQTT_CLIENT.reconnect()
            except ConnectionRefusedError:
                pass
            reconnect += 1
        except (socket.error, mqtt.WebsocketConnectionError):
            pass


while True:
    check_mqtt()
    MQTT_CLIENT.loop()
    now = datetime.datetime.now()
    if start < now.time() < end:
        with requests.get(ADSB_URL, stream=True, timeout=30) as response:
            aircraft = json_stream.requests.load(response, persistent=True)
            aircraft.read_all()
            do_this_thing(aircraft)
